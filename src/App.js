import * as React from "react";
import { useReducer, useEffect } from "react";
import DrawerBar from "./components/Header/DrawerBar";

import Login from "./components/pages/Login";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import ShipmentList from "./components/pages/shipment/ShipmentList";
import Dashboard from "./components/pages/Dashboard";
import NewShipment from "./components/pages/shipment/NewShipment";
import Notices from "./components/pages/communication/Notices";
import PaymentList from "./components/pages/shipment/PaymentList";
import RequestPayment from "./components/pages/shipment/RequestPayment";
import RaisedTicket from "./components/pages/communication/RaisedTicket";
import AllNotifications from "./components/pages/AllNotifications";
import ForgotPassword from "./components/pages/ForgotPassword";
import ShipmentDetails from "./components/pages/shipment/ShipmentDetails";
import CreatePaymentRequest from "./components/pages/shipment/CreatePaymentRequest";
import ResetMailSent from "./components/pages/ResetMailSent";
import ResetPassword from "./components/pages/ResetPassword";
import axios from "axios";
import PaymentDetails from "./components/pages/shipment/PaymentDetails";
import SupportTicketCreate from "./components/pages/communication/SupportTicketCreate";
import CustomerProfie from "./components/Header/Profile/CustomerProfile";
import { ChangePassword } from "./components/Header/Profile/ChangePassword";
import EditProfile from "./components/Header/Profile/EditProfile";
import { DetailRaisedTicket } from "./components/pages/communication/DetailRaisedTicket";

const instance = axios.create({
  // baseURL: "https://staging.sarathilogistics.com",
  baseURL:"http://192.168.1.89:8000",
  headers: {
    "Content-Type": "application/json",
  },
});

//axios interceptor
instance.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem("accessToken");
    if (token) {
      config.headers["Authorization"] = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

instance.interceptors.response.use(
  (response) => {
    return response;
  },
  function (error) {
    const originalRequest = error.config;
    if (error.response.status === 401) {
      const refreshToken = localStorage.getItem("refreshToken");
      return instance
        .post("/api/v1/customer/refresh-token/", {
          refresh: refreshToken,
        })
        .then((res) => {
          if (res.status === 200) {
            // 1) put token to LocalStorage
            const accesstoken = localStorage.setItem(
              "accessToken",
              res.data.access
            );

            // 2) Change Authorization header
            instance.defaults.headers.common["Authorization"] =
              "Bearer " + accesstoken;

            // 3) return originalRequest object with Axios.
            return instance(originalRequest);
          }
        });
    }

    // return Error object with Promise
    return Promise.reject(error);
  }
);

const userDetail = {
  accessToken: "",
  refreshToken: "",
  email: "",
  axiosInstance: instance,
  // serverUrl: "https://staging.sarathilogistics.com",
  // serverUrl:"http://192.168.1.87:8000"
};

export const authContext = React.createContext(userDetail);

function authReducer(state, action) {
  switch (action.type) {
    case "login":
      return {
        ...userDetail,
        email: action.email,
        accessToken: action.accessToken,
        refreshToken: action.refreshToken,
      };
    case "logout":
      localStorage.setItem("email", "");
      localStorage.setItem("accessToken", "");
      localStorage.setItem("refreshToken", "");

      return userDetail;
    default:
      throw state;
  }
}

export default function App() {
  const [newuser, userDispatch] = useReducer(authReducer, userDetail);
  console.log(newuser);
  useEffect(() => {
    const accessToken = localStorage.getItem("accessToken");
    const refreshToken = localStorage.getItem("refreshToken");
    const email = localStorage.getItem("email");
    if (accessToken && refreshToken) {
      userDispatch({
        type: "login",
        accessToken: accessToken,
        refreshToken: refreshToken,
        email: email,
      });
    }
  }, []);

  if (newuser.accessToken) {
    return (
      <authContext.Provider
        value={{ user: newuser, userDispatch: userDispatch }}
      >
        <Router>
          <DrawerBar />
          <Routes>
            <Route path="/" exact element={<Dashboard title="Rider" />} />
            <Route
              path="/shipments"
              exact
              element={<ShipmentList title="Shipment List" />}
            />
            <Route
              path="/new-shipment"
              exact
              element={<NewShipment title="Place New Shipment" />}
            />
            <Route
              path="/notices"
              exact
              element={<Notices title="Notices" />}
            />
            <Route
              path="/payment-list"
              exact
              element={<PaymentList title="Payment List" />}
            />
            <Route
              path="/request-payment"
              exact
              element={<RequestPayment title="Request Payment" />}
            />
            <Route
              path="/raised-ticket"
              exact
              element={<RaisedTicket title="Raised Ticket" />}
            />
            <Route
              path="/notifications"
              exact
              element={<AllNotifications title="Notifications" />}
            />
            <Route
              path="/shipment-/:tracking_code"
              exact
              element={<ShipmentDetails title="Shipment Details" />}
            />
            <Route
              path="/customer/payment-request-create"
              exact
              element={<CreatePaymentRequest title="Request COD Payment" />}
            />
            <Route
              path="/customer/payment-:id-detail"
              exact
              element={<PaymentDetails title="Payment Details" />}
            />
            <Route
              path="/customer/support-tickets-create"
              exact
              element={<SupportTicketCreate title="Raised Ticket" />}
            />
            <Route
              path="/profile/"
              exact
              element={<CustomerProfie title="Customer Profile" />}
            />
            <Route
              path="/password-change/"
              exact
              element={<ChangePassword title="Password Change" />}
            />
            <Route
              path="/profile-update/"
              exact
              element={<EditProfile title="Profile Update" />}
            />
            <Route
              path="/customer/support-tickets-:id"
              exact
              element={<DetailRaisedTicket title="Ticket Detail" />}
            />
          </Routes>
        </Router>
      </authContext.Provider>
    );
  } else {
    return (
      <authContext.Provider
        value={{ user: newuser, userDispatch: userDispatch }}
      >
        <Router>
          <Routes>
            <Route path="/" exact element={<Login />} />
            <Route path="/forgot-password" exact element={<ForgotPassword />} />
            <Route path="/reset-mail-sent" exact element={<ResetMailSent />} />
            <Route path="/reset-password/" exact element={<ResetPassword />} />
          </Routes>
        </Router>
      </authContext.Provider>
    );
  }
}
