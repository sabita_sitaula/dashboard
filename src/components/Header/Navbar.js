import React, { useState, useContext,useEffect } from "react";
import { alpha, makeStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import InputBase from "@material-ui/core/InputBase";
import Badge from "@material-ui/core/Badge";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import SearchIcon from "@material-ui/icons/Search";
import AccountCircle from "@material-ui/icons/AccountCircle";
import NotificationsIcon from "@material-ui/icons/Notifications";
import MoreIcon from "@material-ui/icons/MoreVert";
import Notification from "./Notification";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import { Divider } from "@material-ui/core";
import { Navigate } from "react-router-dom";
import { useNavigate } from "react-router";
import Modal from "@material-ui/core/Modal";
import { Button } from "@material-ui/core";
import { authContext } from "../../App";

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  search: {
    position: "relative",
    display: "flex",
    borderRadius: theme.shape.borderRadius,
    height: "33px",
    marginTop: "8px",
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  inputRoot: {
    color: "inherit",
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
  sectionDesktop: {
    display: "none",
    marginRight: "2px",
    [theme.breakpoints.up("md")]: {
      display: "flex",
    },
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
  modalpaper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    [theme.breakpoints.down("sm")]: {
      left: 0,
      width: 340,
    },
    
  },
  button: {
    color: "white",
    marginTop: "25px",
    marginBottom: "20px",
    textAlign: "center",
  },
}));

export default function Navbar() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);
  const [notiOpen, setNotiOpen] = useState(false);
  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  const navigate = useNavigate();
  const context = useContext(authContext);
  const instance = context.user.axiosInstance;
  const [modalStyle] = useState(getModalStyle);
  const [open, setOpen] = useState(false);
  const [notiBadge,setNotiBadge]= useState("")

  useEffect(() => {
    instance
      .get(`/api/v1/customer/customer-notification-unread-lists`)
      .then((resp) => {
        setNotiBadge(resp.data.total);
      });
  }, []);
  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
    setOpen(true);
  };

  const handleProfileMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
    navigate("/profile/");
  };

  const handleUpdateMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
    navigate("/profile-update/");
  };

  const handleChangeMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
    navigate("/password-change/");
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const handleNotification = (prev) => {
    setNotiOpen((prev) => !prev);
  };

  const handleClickAway = () => {
    setNotiOpen(false);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleLogout = () => {
    context.userDispatch({
      type: "logout"
    })
    navigate("/");
  };

  const handleCancelClick = () => {
    setOpen(false);
  }

  const body = (
    <div style={modalStyle} className={classes.modalpaper}>
      <h2 id="simple-modal-title">Are you sure to logout?</h2>
      <p id="simple-modal-description">You will be logged out from the panel</p>
      <Button
        variant="contained"
        style={{ background: "#319F8D", color: "white" }}
        className={classes.button}
        type="submit"
        onClick={handleLogout}
      >
        Yes, Logout!
      </Button>
      <Button
        variant="contained"
        style={{ background: "#E85253", color: "white", marginLeft:"15px" }}
        className={classes.button}
        type="submit"
        onClick={handleCancelClick}
      >
        Cancel
      </Button>
    </div>
  );

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleProfileMenuClose}>My Profile</MenuItem>
      <MenuItem onClick={handleUpdateMenuClose}>Edit Profile</MenuItem>
      <MenuItem onClick={handleChangeMenuClose}>Change Password</MenuItem>
      <Divider />
      <MenuItem onClick={handleMenuClose}>Logout</MenuItem>

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </Menu>
  );

  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem onClick={handleNotification}>
        <IconButton aria-label="show 11 new notifications" color="inherit">
          <Badge badgeContent={notiBadge} color="secondary">
            <NotificationsIcon />
          </Badge>
        </IconButton>
        <p>Notifications</p>
      </MenuItem>
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
    </Menu>
  );

  return (
    <ClickAwayListener onClickAway={handleClickAway}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <div className={classes.search}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            placeholder="Search…"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            inputProps={{ "aria-label": "search" }}
          />
        </div>
        <div className={classes.sectionDesktop}>
          <IconButton
            aria-label="show 17 new notifications"
            color="inherit"
            onClick={handleNotification}
          >
            <Badge badgeContent={notiBadge} color="secondary">
              <NotificationsIcon />
            </Badge>
          </IconButton>
          <IconButton
            edge="end"
            aria-label="account of current user"
            aria-controls={menuId}
            aria-haspopup="true"
            onClick={handleProfileMenuOpen}
            color="inherit"
          >
            <AccountCircle />
          </IconButton>
        </div>
        <div className={classes.sectionMobile}>
          <IconButton
            aria-label="show more"
            aria-controls={mobileMenuId}
            aria-haspopup="true"
            onClick={handleMobileMenuOpen}
            color="inherit"
          >
            <MoreIcon />
          </IconButton>
        </div>
        <div>
          {renderMobileMenu}
          {renderMenu}
        </div>

        {notiOpen && <Notification />}
      </div>
    </ClickAwayListener>
  );
}
