import React, { useEffect, useState, useContext } from "react";
import { authContext } from "../../App";
import {
  makeStyles,
  List,
  ListItem,
  Divider,
  Typography,
  Button,
} from "@material-ui/core";
import { useNavigate } from "react-router";

const useStyles = makeStyles((theme) => ({
  notification: {
    position: "absolute",
    textAlign: "center",
    background: "green",
    width: "360px",
    fontSize: "20px",
    color: "white",
    maxHeight: "400px",
    overflow: "auto",
    zIndex: 1000,
    right: 50,
    top: 70,
    [theme.breakpoints.down("md")]: {
      width: "290px",
      right: 30,
      top: 58,
    },
  },
}));
function Notification({ notiOpen }) {
  const navigate = useNavigate();
  const context = useContext(authContext);
  const instance = context.user.axiosInstance;
  const [notification, setNotification] = useState([]);
  const classes = useStyles();

  const handleButtonClick = () => {
    navigate("/notifications");
  };

  useEffect(() => {
    instance
      .get(`/api/v1/customer/customer-notification-unread-lists`)
      .then((res) => {
        setNotification(res.data.data);
      })
      .catch((err) => console.log(err));
  }, []);
  return (
    <List className={classes.notification}>
      <Typography>NOTIFICATIONS</Typography>
      <Divider />
      {notification.map((notifi) => (
        <>
          <ListItem button>
            {notifi.created_at} <br /> {notifi.title} <br />
            {notifi.description}
          </ListItem>
          <Divider />
        </>
      ))}
      <Button
        style={{ color: "white", width: "100%" }}
        onClick={handleButtonClick}
      >
        Read all notifications
      </Button>
    </List>
  );
}

export default Notification;
