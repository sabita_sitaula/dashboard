import React, { useEffect, useState, useContext } from "react";
import { authContext } from "../../../App";
import {
  Divider,
  makeStyles,
  Typography,
  List,
  ListItem,
  ListItemText,
  Grid,
  Button,
} from "@material-ui/core";
import { TableContainer, Paper, TextField } from "@material-ui/core";
import { useParams } from "react-router";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";

const useStyles = makeStyles((theme) => ({
  toolbar: {
    minHeight: "56px",
    maxHeight: "56px",
    paddingTop: "15px",
  },
  content: {
    marginLeft: "240px",
    flexGrow: 1,
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      marginLeft: "0px",
    },
  },
  tablecontainer: {
    display: "flex",
    justifyContent: "space-between",
  },
}));

const CustomerProfile = ({ title }) => {
  const [CustomerProfile, setCustomerProfile] = useState({});
  const [ConnectedLogistics, setConnectedLogistics] = useState([]);
  const context = useContext(authContext);
  const instance = context.user.axiosInstance;

  useEffect(() => {
    document.title = title;
    instance
      .get(`/api/v1/customer/profile/`)
      .then((res) => {
        console.log(res.data);
        setCustomerProfile(res.data.data);
        setConnectedLogistics(res.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const classes = useStyles();

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <Grid container spacing={2} className={classes.tablecontainer}>
        <Grid item md={8} xs={12}>
          <TableContainer component={Paper}>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <Typography style={{ padding: "13px" }}>
                CUSTOMER PROFILE: {CustomerProfile.full_name}, LOGISTICS
              </Typography>
            </div>
            <Divider />
            <List component="nav" aria-label="main mailbox folders">
              <ListItem>
                <Grid container lg="auto" spacing={2}>
                  <Grid item md={4} xs={12}>
                    <ListItemText
                      primary="Login Email"
                      secondary={CustomerProfile.email}
                    />
                  </Grid>
                  <Grid item md={4} xs={12}>
                    <ListItemText
                      primary="Contact Number"
                      secondary={CustomerProfile.contact_number}
                    />
                  </Grid>
                  <Grid item md={4} xs={12}>
                    <ListItemText
                      primary="Alternative Contact"
                      secondary={CustomerProfile.alternative_contact_number}
                    />
                  </Grid>
                  <Grid item md={4} xs={12}>
                    <ListItemText
                      primary="Location"
                      secondary={
                        <>
                          {CustomerProfile.address},{CustomerProfile.city},
                          {CustomerProfile.district}
                        </>
                      }
                    />
                  </Grid>
                  <Grid item md={4} xs={12}>
                    <ListItemText
                      primary="Joined on"
                      secondary={CustomerProfile.created_at}
                    />
                  </Grid>
                  <Grid item md={4} xs={12}>
                    <ListItemText
                      primary="Legal Registration"
                      secondary={
                        CustomerProfile.document1
                          ? CustomerProfile.document1
                          : "None"
                      }
                    />
                  </Grid>
                  <Grid item md={4} xs={12}>
                    <ListItemText
                      primary="VAT/PAN Document"
                      secondary={
                        CustomerProfile.document2
                          ? CustomerProfile.document2
                          : "None"
                      }
                    />
                  </Grid>
                  <Grid item md={4} xs={12}>
                    <ListItemText
                      primary="Other Document"
                      secondary={
                        CustomerProfile.document3
                          ? CustomerProfile.document3
                          : "None"
                      }
                    />
                  </Grid>
                </Grid>
              </ListItem>
            </List>
            <Divider />
          </TableContainer>
        </Grid>

        <Grid item lg md={4} xs={12} className={classes.griditem}>
          <TableContainer component={Paper}>
            <Typography
              style={{
                height: "60px",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              PROFILE IMAGE
            </Typography>
            <Divider />
            <Grid container>
              <Grid
                item
                xs={12}
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  
                }}
              >
                <img
                  src={`http://192.168.1.87:8000${CustomerProfile.profile_image}`}
                  alt="User Profile"
                  height="230px"
                  style={{margin:"20px 0px"}}
                />
                {console.log(context)}
              </Grid>
            </Grid>
          </TableContainer>
        </Grid>

        <Grid item md={8} xs={12} className={classes.griditem}>
          <TableContainer component={Paper}>
            <Typography
              style={{
                height: "60px",
                padding: "15px",
                fontSize: "17px",
                fontWeight: "bold",
              }}
            >
              CONNECTIONS
            </Typography>
            <Divider />
            <div style={{ padding: "10px" }}>
              <Typography style={{ fontWeight: "bolder" }}>
                Connected Logistics
              </Typography>
              <small>
                {ConnectedLogistics.connected_logistics} Logistics Unit
              </small>
            </div>
          </TableContainer>
        </Grid>
      </Grid>
    </main>
  );
};

export default CustomerProfile;
