import React, { useState, useEffect, useContext } from "react";
import {
  makeStyles,
  Paper,
  Typography,
  TextField,
  Button,
  Divider,
  MenuItem,
  Grid,
} from "@material-ui/core";
import { authContext } from "../../../App";

const useStyles = makeStyles((theme) => ({
  toolbar: {
    minHeight: "56px",
    maxHeight: "56px",
    paddingTop: "15px",
  },
  content: {
    marginLeft: "240px",
    flexGrow: 1,
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      marginLeft: "0px",
    },
  },
  paper: {
    paddingLeft: "25px",
    paddingRight: "25px",
  },
  typo: {
    marginLeft: "10px",
    marginBottom: "10px",
    marginTop: "10px",
    fontSize: "16px",
    paddingTop: "15px",
    fontWeight: "bold",
  },
  form: {
    marginTop: "25px",
  },
  formControl: {
    width: "50%",
    [theme.breakpoints.down("xs")]: {
      width: "100%",
    },
  },
}));

export const ChangePassword = ({title}) => {
  const newauthContext = useContext(authContext);
  const [email, setEmail] = useState(newauthContext.user.email);
  const [password, setPassword] = useState("");
  const [confirm_password, setConfirm_Password] = useState("");

  const classes = useStyles();
  const instance = newauthContext.user.axiosInstance;

  useEffect(() => {
    document.title = title ;
  })

  const handleSubmit = (e) => {
    e.preventDefault();

    if (password && confirm_password) {
      instance
        .post(`/api/v1/customer/password-change/`, {
          email,
          password,
          confirm_password,
        })
        .then((resp) => {
          console.log(resp.data);
        });
    }
    setPassword("");
    setConfirm_Password("");
  };

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <Paper className={classes.paper}>
        <Typography variant="h6" display="initial" className={classes.typo}>
          Customer Password Change
        </Typography>
        <Divider />
        <form className={classes.form} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                label="Login Email:"
                value={email}
                variant="outlined"
                margin="normal"
                className={classes.formControl}
                disabled
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                label="Password :"
                type="password"
                variant="outlined"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                margin="normal"
                required
                className={classes.formControl}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                label="confirm Password :"
                type="password"
                name="receiver_name"
                variant="outlined"
                value={confirm_password}
                onChange={(e) => setConfirm_Password(e.target.value)}
                margin="normal"
                className={classes.formControl}
                required
              />
            </Grid>
            <Button
              variant="contained"
              style={{
                background: "#319F8D",
                color: "white",
                marginLeft: "11px",
                marginBottom: "20px",
                marginTop: "20px",
              }}
              className={classes.button}
              type="submit"
            >
              Save Changes
            </Button>
          </Grid>
        </form>
      </Paper>
    </main>
  );
};
