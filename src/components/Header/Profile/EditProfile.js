import React, { useEffect, useContext, useState } from "react";
import {
  makeStyles,
  Paper,
  Typography,
  TextField,
  Button,
  Divider,
  MenuItem,
  Grid,
  Box,
} from "@material-ui/core";
import { authContext } from "../../../App";
import Autocomplete from "@material-ui/lab/Autocomplete";

const useStyles = makeStyles((theme) => ({
  toolbar: {
    minHeight: "56px",
    maxHeight: "56px",
    paddingTop: "15px",
  },
  content: {
    marginLeft: "240px",
    flexGrow: 1,
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      marginLeft: "0px",
    },
  },
  paper: {
    paddingLeft: "25px",
    paddingRight: "25px",
  },
  typo: {
    marginLeft: "8px",
    marginTop: "10px",
    fontSize: "19px",
    paddingTop: "15px",
    fontWeight: "bold",
    marginBottom: "20px",
  },
  form: {
    marginTop: "25px",
  },
  formControl: {
    width: "100%",
  },
  uploadBox: {
    padding: "10px",
    marginBottom: "22px",
    display: "flex",
  },
  button: {
    color: "white",
    marginTop: "25px",
    marginBottom: "20px",
    textAlign: "center",
  },
}));

function EditProfile({ title }) {
  const context = useContext(authContext);
  const [city, setCity] = useState([]);
  const [email, setEmail] = useState(context.user.email);
  const classes = useStyles();
  const instance = context.user.axiosInstance;
  const [EditProfile, setEditProfile] = useState({
    full_name: "",
    company_name: "",
    city: "",
    address: "",
    contact_number: "",
    alternative_contact_number: "",
  });
  const [profile_image, setProfileFile] = useState("");
  const [isProfileFilePicked, setIsProfileFilePicked] = useState(false);

  const [file, setFile] = useState("");
  const [isFilePicked, setIsFilePicked] = useState(false);

  const [registrationFile, setRegistrationFile] = useState("");
  const [isRegistrationFilePicked, setIsRegistrationFilePicked] =
    useState(false);

  const [extraFile, setExtraFile] = useState("");
  const [isextraFilePicked, setIsExtraFilePicked] = useState(false);

  useEffect(() => {
    document.title = title;
    instance
      .get(`/api/v1/customer/shipment/city-list/`)
      .then((res) => {
        setCity(res.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const handleCityChange = (e, city) => {
    if (city) {
      var city_id = city.id;
    }
  };

  const handleFile = (e) => {
    setFile(e.target.files[0]);
    setIsFilePicked(true);
  };

  const onInputChange = (e) => {
    setEditProfile({ ...EditProfile, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    // post to server if all data are avaialable
    if (
      EditProfile.full_name &&
      EditProfile.city &&
      EditProfile.contact_number &&
      EditProfile.address
    ) {
      console.log("hi");
      instance
        .patch(`/api/v1/customer/profile-update/`, {
          email,
          EditProfile,
          profile_image,
          registrationFile,
          file,
          extraFile,
        })
        .then((res) => {
          console.log(res.data);
        })
        .catch((err) => console.log(err));
    }
  };

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <Paper className={classes.paper}>
        <Typography variant="h6" display="initial" className={classes.typo}>
          Customer Profile Update
        </Typography>
        <Divider />
        <form className={classes.form} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                label="Login Email:"
                value={email}
                variant="outlined"
                margin="normal"
                className={classes.formControl}
                disabled
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                label="Your Full Name:"
                name="full_name"
                value={EditProfile.full_name}
                onChange={(e) => onInputChange(e)}
                variant="outlined"
                autoFocus
                margin="normal"
                required
                className={classes.formControl}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                label="Your Company Name:"
                value={EditProfile.company_name}
                name="company_name"
                onChange={(e) => onInputChange(e)}
                variant="outlined"
                margin="normal"
                className={classes.formControl}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                label="Your Company Name:"
                value={EditProfile.company_name}
                name="company_name"
                onChange={(e) => onInputChange(e)}
                variant="outlined"
                margin="normal"
                className={classes.formControl}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                label="Primary Contact Number:"
                type="number"
                value={EditProfile.contact_number}
                name="contact_number"
                onChange={(e) => onInputChange(e)}
                variant="outlined"
                margin="normal"
                className={classes.formControl}
                required
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                label="Alternative Contact Number:"
                type="number"
                value={EditProfile.alternative_contact_number}
                name="alternative_contact_number"
                onChange={(e) => onInputChange(e)}
                variant="outlined"
                margin="normal"
                className={classes.formControl}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Autocomplete
                id="combo-box-demo"
                onChange={handleCityChange}
                options={city}
                getOptionLabel={(data) => `${data.name},(${data.district})`}
                renderInput={(params) => (
                  <TextField
                    style={{ marginTop: "15px" }}
                    {...params}
                    label="Receiver City"
                    variant="outlined"
                    value={EditProfile.receiver_city}
                    // onChange={(e)=>setNewShipment(e.target.value)}
                    required
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                label="Your Street Address:"
                variant="outlined"
                name="address"
                value={EditProfile.address}
                onChange={(e) => onInputChange(e)}
                margin="normal"
                required
                className={classes.formControl}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography style={{ marginBottom: "1px" }}>
                Profile Images
              </Typography>
              <Box
                borderColor="primary.main"
                border={1}
                className={classes.uploadBox}
              >
                <Button
                  variant="contained"
                  component="label"
                  style={{ marginRight: "14px" }}
                >
                  Upload File
                  <input
                    accept="image/*"
                    name="profilefile"
                    type="file"
                    onChange={(e) => {
                      setProfileFile(e.target.files[0]);
                      setIsProfileFilePicked(true);
                    }}
                    hidden
                  />
                </Button>
                {isProfileFilePicked && (
                  <>
                    <p>{profile_image.name}</p>
                  </>
                )}
              </Box>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography style={{ marginBottom: "1px" }}>
                Registration Document
              </Typography>
              <Box
                borderColor="primary.main"
                border={1}
                className={classes.uploadBox}
              >
                <Button
                  variant="contained"
                  component="label"
                  style={{ marginRight: "14px" }}
                >
                  Upload File
                  <input
                    accept="image/*"
                    name="registrationfile"
                    type="file"
                    hidden
                    onChange={(e) => {
                      setRegistrationFile(e.target.files[0]);
                      setIsRegistrationFilePicked(true);
                    }}
                  />
                </Button>
                {isRegistrationFilePicked && (
                  <>
                    <p>{registrationFile.name}</p>
                  </>
                )}
              </Box>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography style={{ marginBottom: "1px" }}>VAT/PAN</Typography>
              <Box
                borderColor="primary.main"
                border={1}
                className={classes.uploadBox}
              >
                <Button
                  variant="contained"
                  component="label"
                  style={{ marginRight: "14px" }}
                >
                  Upload File
                  <input
                    accept="image/*"
                    onChange={handleFile}
                    type="file"
                    name="file"
                    hidden
                  />
                </Button>
                {isFilePicked && (
                  <>
                    <p>{file.name}</p>
                  </>
                )}
              </Box>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography style={{ marginBottom: "1px" }}>
                Extra Document
              </Typography>
              <Box
                borderColor="primary.main"
                border={1}
                className={classes.uploadBox}
              >
                <Button
                  variant="contained"
                  component="label"
                  style={{ marginRight: "14px" }}
                >
                  Upload File
                  <input
                    accept="image/*"
                    name="extrafile"
                    type="file"
                    hidden
                    onChange={(e) => {
                      setExtraFile(e.target.files[0]);
                      setIsExtraFilePicked(true);
                    }}
                  />
                </Button>
                {isextraFilePicked && (
                  <>
                    <p>{extraFile.name}</p>
                  </>
                )}
              </Box>
            </Grid>
          </Grid>
          <Button
            variant="contained"
            style={{ background: "#319F8D", color: "white" }}
            className={classes.button}
            type="submit"
          >
            SAVE CHANGES
          </Button>
        </form>
      </Paper>
    </main>
  );
}

export default EditProfile;
