import React, { useContext } from "react";
import {
  AppBar,
  CssBaseline,
  Divider,
  Drawer,
  Hidden,
  IconButton,
  List,
  ListItem,
  ListItemText,
  Toolbar,
  Typography,
  ListItemIcon,
} from "@material-ui/core";
import "./drawerbar.css";
import { authContext } from "../../App";

import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";

import MenuIcon from "@material-ui/icons/Menu";
import PowerSettingsNewIcon from "@material-ui/icons/PowerSettingsNew";
import DashboardIcon from "@material-ui/icons/Dashboard";

import { makeStyles, useTheme } from "@material-ui/core/styles";
import Navbar from "./Navbar";
// import { authContext } from "../../App";
import { menuList, communicationList, settingList } from "../data/menuList";
import { useLocation } from "react-router-dom";
const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  " & .MuiListItemIcon": {
    " & .MuiListItemIcon": {
      color: "#dcdcdc",
    },
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up("sm")]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
    background: "#404e67",
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  // necessary for content to be below app bar
  toolbar: {
    minHeight: "56px",
    maxHeight: "56px",
    paddingTop: "15px",
  },
  drawerPaper: {
    width: drawerWidth,
    backgound: "#404e67",
  },
  brand: {
    textAlign: "center",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  typography: {
    padding: "8px 16px 0",
    fontWeight: "bold",
  },
  sidebar: {
    background: "#404e67",
    color: "#dcdcdc",
  },
  link: {
    textDecoration: "none",
    color: "#dcdcdc",
  },
  active: {
    background: "#3d4a61",
  },
}));

const DrawerBar = (props) => {
let navigate = useNavigate();
  const context = useContext(authContext);
  console.log(context)
  const { window } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  let location = useLocation();
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const handleLogout = () => {
    context.userDispatch({
      type: "logout"
    })
    navigate("/");
  };

  const drawer = (
    <div className={classes.sidebar}>
      <div className={classes.toolbar}>
        <Typography className={classes.brand} variant="h5" component="h1">
          Rider
        </Typography>
      </div>
      <Divider />
      <List>
        <Link to="/" className={classes.link}>
          <ListItem
            button
            className={location.pathname === "/" ? classes.active : null}
          >
            <ListItemIcon className="icon">
              <DashboardIcon />
            </ListItemIcon>
            <ListItemText> Dashboard </ListItemText>
          </ListItem>
        </Link>
      </List>
      <Divider />
      <Typography className={classes.typography}>SHIPMENT & PAYMENT</Typography>
      <List>
        {menuList.map((text, index) => (
          <Link key={text.menuLink} className={classes.link} to={text.menuLink}>
            <ListItem
              button
              key={text.menuName}
              className={
                location.pathname === text.menuLink ? classes.active : null
              }
            >
              <ListItemIcon className="icon">{text.icon}</ListItemIcon>
              <ListItemText primary={text.menuName} />
            </ListItem>
          </Link>
        ))}
      </List>

      <Typography className={classes.typography}>COMMUNICATION</Typography>
      <List>
        {communicationList.map((text, index) => (
          <Link key={text.menuLink} className={classes.link} to={text.menuLink}>
            <ListItem
              button
              key={text.menuName}
              className={
                location.pathname === text.menuLink ? classes.active : null
              }
            >
              <ListItemIcon className="icon">{text.icon}</ListItemIcon>
              <ListItemText primary={text.menuName} />
            </ListItem>
          </Link>
        ))}
      </List>

      <Typography className={classes.typography}>SETTINGS AND GUIDE</Typography>
      <List>
        {settingList.map((text, index) => (
          <Link key={text.menuLink} className={classes.link} to={text.menuLink}>
            <ListItem
              button
              key={text.menuName}
              className={
                location.pathname === text.menuLink ? classes.active : null
              }
            >
              <ListItemIcon className="icon">{text.icon}</ListItemIcon>
              <ListItemText primary={text.menuName} />
            </ListItem>
          </Link>
        ))}
      </List>
      <Divider />
      <List>
        <ListItem button>
          <ListItemIcon className="icon">
            <PowerSettingsNewIcon />
          </ListItemIcon>
          <ListItemText onClick={handleLogout}>Logout</ListItemText>
        </ListItem>
      </List>
    </div>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar style={{justifyContent:"flex-end"}}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Navbar />
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer}>
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === "rtl" ? "right" : "left"}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
    </div>
  );
};

export default DrawerBar;
