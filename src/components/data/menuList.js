import LocalShippingIcon from "@material-ui/icons/LocalShipping";
import ViewStreamIcon from "@material-ui/icons/ViewStream";
import PublishIcon from "@material-ui/icons/Publish";
import CreditCardIcon from "@material-ui/icons/CreditCard";
import ReceiptIcon from "@material-ui/icons/Receipt";

import RecordVoiceOverIcon from '@material-ui/icons/RecordVoiceOver';
import MoneyIcon from '@material-ui/icons/Money';

import SettingsIcon from '@material-ui/icons/Settings';
import HelpIcon from '@material-ui/icons/Help';

export const menuList = [
  {
    menuName: "Shipment List",
    icon: <ViewStreamIcon />,
    menuLink: "/shipments",
  },
  {
    menuName: "Place New Shipment",
    icon: <LocalShippingIcon />,
    menuLink: "/new-shipment",
  },
  {
    menuName: "Bulk Upload",
    icon: <PublishIcon />,
    menuLink: "/bulk-upload",
  },
  {
    menuName: "Request Payment",
    icon: <CreditCardIcon />,
    menuLink: "/request-payment",
  },
  {
    menuName: "Payment List",
    icon: <ReceiptIcon />,
    menuLink: "/payment-list",
  },
];

export const communicationList = [
  {
    menuName: "Notices",
    icon: <RecordVoiceOverIcon />,
    menuLink: "/notices"
  },
  {
    menuName: "Raised Tickets",
    icon: <MoneyIcon />,
    menuLink: "/raised-ticket"
  }
];

export const settingList = [
  {
    menuName: "Settings",
    icon: <SettingsIcon />,
    menuLink: "/settings"
  },
  {
    menuName: "How to",
    icon: <HelpIcon />,
    menuLink: "/how-to"
  },
 
  
];

