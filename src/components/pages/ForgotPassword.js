import React, { useState, useContext } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Link } from "react-router-dom";
import { authContext } from "../../App";
import { useNavigate } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import {
  Button,
  TextField,
  Typography,
  FormGroup,
  InputAdornment,
  Grid,
  Paper,
} from "@material-ui/core";
import MailOutlineIcon from "@material-ui/icons/MailOutline";

const useStyles = makeStyles({
  login: {
    position: "relative",
    padding: "30px",
  },
  loadingImg: {
    position: "absolute",
    marginLeft: "auto",
    marginRight: "auto",
    top: 95,
    left: 0,
    right: 0,
    textAlign: "center",
  },
  pwForm: {
    margin: "87px auto",
    width: 350,
    height: "60vh",
    padding: " 20px 20px 0",
  },
  pwTitle: {
    margin: "20px 0px",
  },
  pwEmail: {
    margin: "15px 0px 20px",
  },
  loginLink: {
    color: "#3F51B5",
    textDecoration: "none",
    "&:hover": {
      color: "darkBlue",
    },
  },
});

const ForgotPassword = () => {
  const [email, setEmail] = useState("");
  const [disable, setDisable] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState("");
  const context = useContext(authContext);
  let navigate = useNavigate();
  const classes = useStyles();
  const instance = context.user.axiosInstance;

  const handleSubmit = (e) => {
    e.preventDefault();

    if (email) {
      setIsLoading(true);
      instance
        .post(`/api/v1/customer/password-forgot/`, {
          email: email,
        })
        .then((resp) => {
          if (resp.data.status == "success") {
            setIsLoading(false);
            navigate("/reset-mail-sent");
          }
        })
        .catch((err) => {
          setIsLoading(false);
          setError(err.response.data.email);
        });
    }
    setDisable(true);
  };

  return (
    <Grid container justifyContent="center" spacing={3}>
      <Grid item sm={4} md={4} lg={4}>
        <Paper className={classes.login}>
          {isLoading && <CircularProgress className={classes.loadingImg} />}
          <Typography
            className={classes.pwTitle}
            align="center"
            variant="h6"
            display="initial"
          >
            Reset link will be sent to your entered email.
          </Typography>
          <form onSubmit={handleSubmit}>
            <FormGroup>
              <TextField
                disabled={disable}
                className={classes.pwEmail}
                label="Email address"
                placeholder="enter your email"
                variant="outlined"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                autoFocus
                type="email"
                required
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <MailOutlineIcon />
                    </InputAdornment>
                  ),
                  pattern: "[a-z]{1,15}",
                }}
              />
              <Button
                disabled={disable}
                variant="contained"
                color="primary"
                type="submit"
              >
                Submit
              </Button>
            </FormGroup>
          </form>
          <small
            style={{
              display: "flex",
              justifyContent: "flex-start",
              margin: "15px 0 0 10px",
              textTransform: "capitalize",
            }}
          >
            <Link className={classes.loginLink} to="/">
              &#60;&#60; go to login page
            </Link>
          </small>
          {error && (
            <div
              style={{
                color: "red",
                marginTop: "20px",
                width: "100%",
                textTransform: "capitalize",
              }}
            >
              email doesnot exist!!!
            </div>
          )}
        </Paper>
      </Grid>
    </Grid>
  );
};

export default ForgotPassword;
