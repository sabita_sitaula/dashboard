import React, { useState, useContext, useEffect } from "react";
import { authContext } from "../../App";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import { Button, TextField, Typography } from "@material-ui/core";
import FormGroup from "@material-ui/core/FormGroup";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import InputAdornment from "@material-ui/core/InputAdornment";
import LockIcon from "@material-ui/icons/Lock";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  buttonstyle: {
    marginTop: "20px",
  },
  typographyStyle: {
    marginTop: "5px",
  },
  forgotLink: {
    color: "#3F51B5",
    textDecoration: "none",
    marginTop: "10px",
    "&:hover": {
      color: "darkBlue",
    },
  },
  loadingImg: {
    position: "absolute",
    marginLeft: "auto",
    marginRight: "auto",
    top: 95,
    left: 0,
    right: 0,
    textAlign: "center",
  },
  login: {
    position: "relative",
    padding: "30px",
  },
}));

function Login() {
  const newauthContext = useContext(authContext);
  const [disable, setDisable] = useState(false);
  const [isloading, setIsLoading] = useState(false);

  const [email, setEmail] = useState("praweshkhaling21@gmail.com");
  const [password, setPassword] = useState("123456");

  // const [email, setEmail] = useState("prabinchy1234@gmail.com");
  // const [password, setPassword] = useState("prabin1234");

  // const [email, setEmail] = useState("sabitasitaula705@gmail.com");
  // const [password, setPassword] = useState("Sabita@123");

  const classes = useStyles();
  const instance = newauthContext.user.axiosInstance;


  const handleSubmit = (e) => {
    e.preventDefault();

    if (email && password) {
      setIsLoading(true);
      instance
        .post(`/api/v1/customer/login/`, {
          email: email,
          password: password,
        })
        .then((resp) => {
          if (resp.data.access && resp.data.refresh) {
            setIsLoading(false);
            localStorage.setItem("accessToken", resp.data.access);
            localStorage.setItem("refreshToken", resp.data.refresh);
            localStorage.setItem("email", resp.data.email);
            newauthContext.userDispatch({
              type: "login",
              email: resp.data.email,
              accessToken: resp.data.access,
              refreshToken: resp.data.refresh,
            });
          }
        });
    }
    setDisable(true);
    setEmail("");
    setPassword("");
  };

  return (
    <Grid container justifyContent="center" spacing={3}>
      <Grid item sm={4} md={4} lg={4}>
        <Paper className={classes.login}>
          {isloading && <CircularProgress className={classes.loadingImg} />}
          <Typography
            align="center"
            variant="h5"
            display="initial"
            className={classes.typographyStyle}
          >
            Login Here
          </Typography>
          <form onSubmit={handleSubmit}>
            <FormGroup>
              <TextField
                label="Email address"
                disabled={disable}
                variant="outlined"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                autoFocus
                margin="normal"
                type="email"
                required
                pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <MailOutlineIcon />
                    </InputAdornment>
                  ),
                  pattern: "[a-z]{1,15}",
                }}
              />
              <TextField
                disabled={disable}
                type="password"
                label="Password"
                variant="outlined"
                margin="normal"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <LockIcon />
                    </InputAdornment>
                  ),
                }}
              />
              <Button
                disabled={disable}
                className={classes.buttonstyle}
                variant="contained"
                color="primary"
                type="submit"
              >
                Submit
              </Button>
            </FormGroup>
          </form>
          <small
            style={{
              display: "flex",
              justifyContent: "flex-end",
              margin: "10px 5px 0px 0px",
              textTransform: "capitalize",

              fontWeight: "bold",
            }}
          >
            <Link className={classes.forgotLink} to="/forgot-password">
              forgot password?
            </Link>
          </small>
        </Paper>
      </Grid>
    </Grid>
  );
}

export default Login;
