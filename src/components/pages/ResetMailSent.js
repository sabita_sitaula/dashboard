import React from "react";
import Alert from "@material-ui/lab/Alert";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";

const ResetMailSent = () => {
  return (
    <Alert
      iconMapping={{ success: <CheckCircleOutlineIcon fontSize="inherit" /> }}
    >
      reset link is sent to your email
    </Alert>
  );
};

export default ResetMailSent;
