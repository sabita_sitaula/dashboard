import React, { useEffect, useState, useContext } from "react";
import {
  makeStyles,
  TableContainer,
  Paper,
  Typography,
} from "@material-ui/core";
import MuiAccordion from "@material-ui/core/Accordion";
import MuiAccordionSummary from "@material-ui/core/AccordionSummary";
import MuiAccordionDetails from "@material-ui/core/AccordionDetails";
import { withStyles } from "@material-ui/core/styles";
import { authContext } from "../../../App";
import CircularProgress from '@material-ui/core/CircularProgress';

const Accordion = withStyles({
  root: {
    border: "1px solid rgba(0, 0, 0, .125)",
    boxShadow: "none",
    "&:not(:last-child)": {
      borderBottom: 0,
    },
    "&:before": {
      display: "none",
    },
    "&$expanded": {
      margin: "auto",
    },
  },
  expanded: {},
})(MuiAccordion);

const AccordionSummary = withStyles({
  root: {
    backgroundColor: "rgba(0, 0, 0, .03)",
    borderBottom: "1px solid rgba(0, 0, 0, .125)",
    marginBottom: -1,
    minHeight: 56,
    "&$expanded": {
      minHeight: 56,
    },
  },
  content: {
    "&$expanded": {
      margin: "12px 0",
    },
  },
  expanded: {},
})(MuiAccordionSummary);

const AccordionDetails = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiAccordionDetails);

const useStyles = makeStyles((theme) => ({
  // necessary for content to be below app bar
  toolbar: {
    minHeight: "56px",
    maxHeight: "56px",
    paddingTop: "15px",
  },
  content: {
    marginLeft: "240px",
    flexGrow: 1,
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      marginLeft: "0px",
    },
  },
  link: {
    textDecoration: "none",
    marginBottom: "15px",
  },
  typo: {
    marginBottom:"20px"
  },
  accordion: {
    color:"green"
  },
  loading: {
    position: "absolute",
    margin: "auto",
    left: 0,
    right: 0,
    top:200
  }
}));
const Notices = ({title}) => {
  const [Notices, setNotices] = useState([]);
  const context = useContext(authContext);
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(false);
  const instance = context.user.axiosInstance;

  useEffect(() => {
    setIsLoading(true);
    document.title = title;
    instance
      .get(`/api/v1/customer/customer-notice/`)
      .then((res) => {
        setIsLoading(false);
        setNotices(res.data);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <h3>RECENT NOTICES</h3>
        <Typography className={classes.typo}>All notices from your service providerss</Typography>
      <TableContainer component={Paper}>

        {isLoading && <CircularProgress className={classes.loading} /> }
        {Notices.map((notice) => (
          <li key={notice.id} type="none">
            <Accordion>
              <AccordionSummary >
                <Typography className={classes.accordion}>
                  {notice.id}. BY:
                  {notice.logistics_branch}&nbsp;({notice.created_at}{" "}|{" "}
                  <span style={{color:"red"}}>{notice.date_ago})</span>
                </Typography>
              </AccordionSummary>
              <AccordionDetails>
                <Typography>{notice.notice}</Typography>
              </AccordionDetails>
            </Accordion>
          </li>
        ))}
      </TableContainer>
    </main>
  );
};

export default Notices;
