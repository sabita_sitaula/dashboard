import React, { useEffect, useState, useContext } from "react";
import { authContext } from "../../../App";
import { Button, makeStyles } from "@material-ui/core";
import { useNavigate } from "react-router-dom";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
  Typography,
  Divider,
} from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";
import CircularProgress from '@material-ui/core/CircularProgress';
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
  toolbar: {
    minHeight: "56px",
    maxHeight: "56px",
    paddingTop: "15px",
  },
  content: {
    marginLeft: "240px",
    flexGrow: 1,
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      marginLeft: "0px",
    },
  },
  table: {
    minWidth: 650,
  },
  tablecell: {
    fontWeight: "bold",
  },
  box: {
    flexGrow: 1,
    marginBottom: "30px",
    marginTop: "20px",
    borderColor: "black",
  },
  griditem: {
    height: "140px",
    width: "50px",
  },

  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    height: "100px",
    width: "170px",
    fontSize: "20px",
    color: "#319f77",
    [theme.breakpoints.down("xs")]: {
      width:"134px"
    },
  },
  button: {
    marginTop: "22px",
    color: "white",
  },
  typo: {
    fontSize: "18px",
    marginLeft: "15px",
    marginTop: "20px",
    marginBottom: "30px",
  },
  loading: {
    position: "absolute",
    marginLeft: "auto",
    marginRight:"auto",
    left: 0,
    right: 0,
    top: 300,
    [theme.breakpoints.down("xs")]: {
      top: 450,
      right:420
    },
    [theme.breakpoints.down("md")]: {
      top: 420,
      left:200
    },
  },
  link: {
    textDecoration: "none",
    color: "#319F8D",
    "&:hover": {
      color:"#2C5C2D"
    }
  }
}));

const RaisedTicket = ({ title }) => {
  const navigate = useNavigate();
  const [RaisedTicket, setRaisedTicket] = useState([]);
  const [Ticket, setTicket] = useState([]);
  const [totalPage, setTotalPage] = useState([]);
  const context = useContext(authContext);
  const classes = useStyles();
  const [page, setPage] = React.useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const instance = context.user.axiosInstance;

  useEffect(() => {
    setIsLoading(true);
    document.title = title;
    fetchData();
  }, []);
  const handleChange = (event,value) => {
    setPage(value);
    fetchData();
  };
  const fetchData = () => {
    instance
      .get(
        `/api/v1/customer/customer-support-ticket-list?page=${page}`
      )
      .then((res) => {
        setIsLoading(false);
        setRaisedTicket(res.data.data);
        setTicket(res.data);
        setTotalPage(res.data.last_page);
      })
      .catch((err) => console.log(err));
  };

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />

      <div className={classes.box}>
        <Grid container spacing={3}>
          <Grid item lg md={4} xs={6} className={classes.griditem}>
            <Paper className={classes.paper}>
              <Link to ="/" className={classes.link}>Open Tickets Raised ({Ticket.open_tickets})</Link>
            </Paper>
          </Grid>
          <Grid item lg md={4} xs={6} className={classes.griditem}>
            <Paper className={classes.paper}>
            <Link to ="/" className={classes.link}>Reviewing Tickets ({Ticket.reviewing_tickets})</Link>
            </Paper>
          </Grid>
          <Grid item lg md={4} xs={6} className={classes.griditem}>
            <Paper className={classes.paper}>
            <Link to ="/" className={classes.link}>Solved Tickets ({Ticket.solved_tickets})</Link>
            </Paper>
          </Grid>
          <Grid item lg md={4} xs={6} className={classes.griditem}>
            <Button
              variant="contained"
              style={{ background: "#319F8D", color: "white" }}
              className={classes.button}
              onClick={()=>navigate('/customer/support-tickets-create/')}
            >
              Raise new Ticket
            </Button>
          </Grid>
        </Grid>
      </div>
      <TableContainer component={Paper}>
        <Typography className={classes.typo}>All Tickets</Typography>
        <Divider />
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell className={classes.tablecell}>
                Ticket&nbsp;Id
              </TableCell>
              <TableCell align="left" className={classes.tablecell}>
                Raised&nbsp;To
              </TableCell>
              <TableCell align="left" className={classes.tablecell}>
                Raised&nbsp;Date
              </TableCell>
              <TableCell align="left" className={classes.tablecell}>
                Ticket&nbsp;Status
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {isLoading && <CircularProgress className={classes.loading} /> }
            {RaisedTicket.map((ticket) => (
              <TableRow key={ticket.id}>
                <TableCell component="th" scope="row">
                  <Link to={`/customer/support-tickets-${ticket.id}`} className={classes.link}>{ticket.ticket_id}</Link>
                </TableCell>
                <TableCell align="left">{ticket.raised_to}</TableCell>
                <TableCell align="left">{ticket.created_at}</TableCell>
                <TableCell align="left"><span style={{background:"#FFA87D", color:"white",padding:"5px", borderRadius:"5px"}}>{ticket.ticket_status}</span></TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <div className={classes.root}>
        <Typography>Page: {page}</Typography>
        <Pagination count={totalPage} page={page} onChange={handleChange} />
      </div>
    </main>
  );
};

export default RaisedTicket;
