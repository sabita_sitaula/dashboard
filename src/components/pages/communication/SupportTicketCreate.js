import React, { useState, useEffect, useContext } from "react";
import {
  Button,
  TextareaAutosize,
  TextField,
  makeStyles,
  Paper,
  Typography,
  Divider,
  Grid,
  Box,
  MenuItem,
} from "@material-ui/core";
import { authContext } from "../../../App";
import { useNavigate } from "react-router";

const useStyles = makeStyles((theme) => ({
  paper: {
    paddingLeft: "25px",
    paddingRight: "25px",
  },
  content: {
    marginLeft: "240px",
    flexGrow: 1,
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      marginLeft: "0px",
    },
  },
  toolbar: {
    minHeight: "56px",
    maxHeight: "56px",
    paddingTop: "15px",
  },
  typo: {
    marginLeft: "25px",
    marginTop: "10px",
    fontSize: "19px",
    paddingTop: "15px",
  },
  formControl: {
    width: "100%",
  },
  msg: {
    width: "100%",
    marginBottom: "20px",
  },
  uploadBox: {
    padding: "10px",
    marginBottom: "22px",
    display: "flex",
  },
}));
const SupportTicketCreate = ({ title }) => {
  const [raiseNewTicket, setRaiseNewTicket] = useState([]);
  const classes = useStyles();
  const context = useContext(authContext);
  const instance = context.user.axiosInstance;
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const [raised_to, setRaisedTo] = useState([]);
  const [issue_detail, setIssueDetail] = useState([]);
  const [related_file, setRelatedFile] = useState([]);
  const [isFilePicked, setIsFilePicked] = useState(false);

  useEffect(() => {
    // setIsLoading(true);
    document.title = title;
    instance
      .get(`/api/v1/customer/customer-support-ticket-list/`)
      .then((resp) => {
        console.log(resp.data);
        // setIsLoading(false);
        setRaiseNewTicket(resp.data.data);
      });
  }, []);

  const handleFormSubmit = (e) => {
    e.preventDefault();
    if (raised_to && issue_detail) {
      instance
        .post(`/api/v1/customer/customer-support-ticket-create/`, {
          raised_to,
          issue_detail,
          related_file,
        })
        .then((res) => {
          console.log(res);
        })
        .catch((err) => console.log(err));
      console.log("form is submitted");
    }
  };
  const handleFile = (e) => {
    setRelatedFile(e.target.files[0]);
    setIsFilePicked(true);
  };
  // const handleButtonClick = () => {
  //   navigate("/raised-ticket");
  // };

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <Paper className={classes.paper}>
        <Typography variant="h6" display="initial" className={classes.typo}>
          REQUEST COD Payment
        </Typography>
        <small
          style={{
            display: "inline-block",
            color: "red",
            marginLeft: "25px",
            marginBottom: "35px",
          }}
        >
          Fields marked with '*' are required fields.
        </small>
        <Divider />
        <form className={classes.form} onSubmit={handleFormSubmit}>
          <Grid container>
            <Grid item xs={12}>
              <TextField
                id="select"
                name="raised_to"
                value={raised_to}
                onChange={(e) => setRaisedTo(e.target.value)}
                label=" Select Service Provider:"
                variant="outlined"
                margin="normal"
                select
                required
                className={classes.formControl}
              >
                {raiseNewTicket.map((data) => (
                  <MenuItem value={data.id} key={data.id}>
                    {data.raised_to}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={12}>
              <Typography>Your issue *</Typography>
              <TextareaAutosize
                className={classes.msg}
                name="issue_detail"
                value={issue_detail}
                onChange={(e) => setIssueDetail(e.target.value)}
                aria-label="minimum height"
                minRows={10}
                placeholder="Your issue Detail"
                style={{ color: "#B1C1D8", marginTop: "7px" }}
                required
              />
            </Grid>

            <Grid item xs={12}>
              <Typography style={{ marginBottom: "1px" }}>
                Related File
              </Typography>
              <Box
                borderColor="primary.main"
                border={1}
                className={classes.uploadBox}
              >
                <Button
                  style={{ marginRight: "20px" }}
                  variant="contained"
                  component="label"
                >
                  Upload File
                  <input
                    accept="image/*"
                    type="file"
                    name="related_file"
                    onChange={handleFile}
                    hidden
                  />
                </Button>
                {isFilePicked && (
                  <div>
                    <p> {related_file.name}</p>
                  </div>
                )}
              </Box>
            </Grid>
          </Grid>
          <Button
            variant="contained"
            style={{
              background: "#319F8D",
              color: "white",
              marginBottom: "18px",
            }}
            className={classes.button}
            type="submit"
            // onClick={handleButtonClick}
          >
            Raise a Ticket
          </Button>
        </form>
      </Paper>
    </main>
  );
};

export default SupportTicketCreate;
