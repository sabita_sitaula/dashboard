import React, { useEffect, useState, useContext } from "react";
import { authContext } from "../../../App";
import {
  Divider,
  makeStyles,
  Typography,
  List,
  ListItem,
  ListItemText,
  Grid,
  Button,
} from "@material-ui/core";
import { TableContainer, Paper, TextField } from "@material-ui/core";
import { useParams } from "react-router";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  toolbar: {
    minHeight: "56px",
    maxHeight: "56px",
    paddingTop: "15px",
  },
  content: {
    marginLeft: "240px",
    flexGrow: 1,
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      marginLeft: "0px",
    },
  },
  tablecontainer: {
    display: "flex",
    justifyContent: "space-between",
  },
}));

export const DetailRaisedTicket = ({ title }) => {
  const [detailRaisedTicket, setDetailRaisedTicket] = useState({});
  const classes = useStyles();
  const context = useContext(authContext);
  const { id } = useParams();
  const instance = context.user.axiosInstance;

  useEffect(() => {
    document.title = title;
    instance
      .get(`/api/v1/customer/customer-support-ticket-detail/${id}`)
      .then((res) => {
        setDetailRaisedTicket(res.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <Grid container spacing={2} className={classes.tablecontainer}>
        <Grid item md={8} xs={12}>
          <TableContainer component={Paper}>
            <Typography style={{ padding: "13px" }}>Ticket Details</Typography>
            <Divider />
            <List component="nav" aria-label="main mailbox folders">
              <ListItem>
                <Grid container lg="auto" spacing={2}>
                  <Grid item md={6}>
                    <ListItemText
                      primary="Ticket ID"
                      secondary={detailRaisedTicket.id}
                    />
                  </Grid>
                  <Grid item md={6}>
                    <ListItemText
                      primary="Ticket Raised To"
                      secondary={detailRaisedTicket.raised_to}
                    />
                  </Grid>
                  <Grid item md={6}>
                    <ListItemText
                      primary="Ticket Status"
                      secondary={detailRaisedTicket.ticket_status}
                    />
                  </Grid>
                  <Grid item md={6}>
                    <ListItemText
                      primary="Raised On"
                      secondary={detailRaisedTicket.created_at}
                    />
                  </Grid>
                  <Grid item md={6}>
                    {detailRaisedTicket.related_file === "No file available" ? (
                      <ListItemText
                        primary="Attached File"
                        secondary={detailRaisedTicket.related_file}
                      />
                    ) : (
                      <ListItemText
                        primary="Attached File"
                        secondary={
                          <Link
                            to={{
                              pathname: `{/http://192.168.1.89:8000${detailRaisedTicket.related_file}`,
                            }}
                            target="_blank"
                            style={{
                              textDecoration: "none",
                              color: "#319F8D,",
                            }}
                          >
                            View File
                          </Link>
                        }
                      />
                    )}
                  </Grid>
                  <Grid item md={6}>
                    <ListItemText
                      primary="Issue Detail"
                      secondary={detailRaisedTicket.issue_detail}
                    />
                  </Grid>
                </Grid>
              </ListItem>
            </List>
          </TableContainer>
        </Grid>

        <Grid item lg md={4} xs={12} className={classes.griditem}>
          <TableContainer component={Paper}>
            <Typography
              style={{
                height: "60px",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              FOLLOWUP CONVERSATIONS
            </Typography>
            <Divider />
            <div style={{ height: "290px" }}></div>
            <Grid container>
              <Grid item xs={12}>
                <TextareaAutosize
                  autoFocus
                  aria-label="minimum height"
                  minRows={5}
                  placeholder="Write your Remarks"
                  style={{ marginLeft: "40px", width: "250px" }}
                />
              </Grid>
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  style={{
                    marginLeft: "40px",
                    marginBottom: "20px",
                    marginTop: "10px",
                    background: "#319F8D",
                    color: "white",
                  }}
                >
                  Add Remarks
                </Button>
              </Grid>
            </Grid>
          </TableContainer>
        </Grid>
      </Grid>
    </main>
  );
};
