import React, { useEffect, useState, useContext } from "react";
import { authContext } from "../../App";
import {
  Button,
  Divider,
  makeStyles,
  Typography,
} from "@material-ui/core";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  TablePagination,
  TextField,
} from "@material-ui/core";

import { useNavigate } from "react-router";

const useStyles = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
  toolbar: {
    minHeight: "56px",
    maxHeight: "56px",
    paddingTop: "15px",
  },
  content: {
    marginLeft: "240px",
    flexGrow: 1,
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      marginLeft: "0px",
    },
  },
  table: {
    minWidth: 650,
  },
  tablecell: {
    fontWeight: "bold",
  },
  box: {
    flexGrow: 1,
    marginBottom: "30px",
    marginTop: "20px",
    borderColor: "black",
  },
  griditem: {
    height: "140px",
    width: "40px",
  },

  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    height: "100px",
    width: "130px",
    fontSize: "20px",
    color: "white",
  },
  typo: {
    fontSize: "18px",
    marginLeft: "15px",
    marginTop: "20px",
    marginBottom: "30px",
  },
}));

const AllNotifications = (props) => {
  const [AllNotifications, setAllNotifications] = useState([]);
  const [otherLists, setOtherLists] = useState([]);
  const context = useContext(authContext);
  const { title } = props;
  const instance = context.user.axiosInstance;
  
  const navigate = useNavigate();

  useEffect(() => {
    document.title = title;
    instance
      .get(
        `/api/v1/customer/customer-notification-lists/`
      )
      .then((res) => {
        setAllNotifications(res.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleView = (not_id) => {
    instance
      .get(
        `/api/v1/customer/customer-notification-read-${not_id}/`
      )
      .then((res) => {
        setOtherLists(res.data.data);
        if (res.data.data.tracking_code) {
          navigate(`/shipment-/${res.data.data.tracking_code}`);
        }
      })
      .catch((err) => console.log(err));
  };

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <TableContainer component={Paper}>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Typography style={{ paddingTop: "13px" }} className={classes.typo}>
            Notifications
          </Typography>
          <TextField
            style={{ marginLeft: "6px" }}
            inputProps={{
              style: {
                padding: "9.5px 14px",
              },
            }}
            id="outlined-basic"
            placeholder="type keyword & enter"
            variant="outlined"
          />
        </div>
        <Divider />
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell className={classes.tablecell}>Date</TableCell>
              <TableCell align="left" className={classes.tablecell}>
                Title
              </TableCell>
              <TableCell align="left" className={classes.tablecell}>
                Details
              </TableCell>
              <TableCell align="left" className={classes.tablecell}>
                Action
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {AllNotifications.slice(
              page * rowsPerPage,
              page * rowsPerPage + rowsPerPage
            ).map((notification) => (
              <TableRow key={notification.created_at} hover>
                <TableCell component="th" scope="row">
                  {notification.created_at}
                </TableCell>
                <TableCell align="left">{notification.title}</TableCell>
                <TableCell align="left">{notification.description}</TableCell>
                <TableCell align="left">
                  {notification.is_seen === true ? (
                    <Button
                      style={{ background: "#319F8D", color: "white" }}
                      onClick={() => {
                        handleView(notification.id);
                      }}
                    >
                      View
                    </Button>
                  ) : (
                    <Button
                      style={{ background: "red", color: "white" }}
                      onClick={() => {
                        handleView(notification.id);
                      }}
                    >
                      View
                    </Button>
                  )}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10]}
        component="div"
        count={AllNotifications.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </main>
  );
};

export default AllNotifications;
