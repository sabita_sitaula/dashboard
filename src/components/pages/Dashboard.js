import React, { useState, useEffect, useContext } from "react";
import { authContext } from "../../App";
import { Button, makeStyles } from "@material-ui/core";
import { Paper, Grid, Typography, TableContainer } from "@material-ui/core";
import PublishIcon from "@material-ui/icons/Publish";
import RotateLeftIcon from "@material-ui/icons/RotateLeft";
import AirportShuttleIcon from "@material-ui/icons/AirportShuttle";
import CancelIcon from "@material-ui/icons/Cancel";
import DoneIcon from "@material-ui/icons/Done";
import KeyboardReturnIcon from "@material-ui/icons/KeyboardReturn";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import FormatListBulletedIcon from "@material-ui/icons/FormatListBulleted";
import AccountBalanceWalletIcon from "@material-ui/icons/AccountBalanceWallet";
import MoneyIcon from "@material-ui/icons/Money";
import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";

const useStyles = makeStyles((theme) => ({
  // necessary for content to be below app bar
  toolbar: {
    minHeight: "56px",
    maxHeight: "56px",
    paddingTop: "15px",
  },
  content: {
    marginLeft: "240px",
    flexGrow: 1,
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      marginLeft: "0px",
    },
  },
  tablecontainer: {
    display: "flex",
    justifyContent: "space-between",
  },
  box: {
    flexGrow: 1,
    marginBottom: "30px",
    marginTop: "20px",
    borderColor: "black",
  },
  griditem: {
    height: "140px",
    width: "40px",
  },

  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    height: "100px",
    width: "230px",
    fontSize: "17px",
    color: "white",
    [theme.breakpoints.down("xs")]: {
      width: "100%",
    },
    [theme.breakpoints.down("sm")]: {
      width: "100%",
    },
  },
}));
const Dashboard = ({ title }) => {
  const [dashboard, setDashboard] = useState([]);
  const [shipmentOverview, setShipmentOverview] = useState([]);
  const [paymentsOverview, setPaymentsOverview] = useState([]);

  const context = useContext(authContext);
  const classes = useStyles();
  const instance = context.user.axiosInstance;

  useEffect(() => {
    document.title = title;
    instance
      .get(`/api/v1/customer/dashboard-shipment-overview/`)
      .then((res) => {
        setShipmentOverview(res.data);
      })
      .catch((err) => console.log(err));

    instance
      .get(`/api/v1/customer/dashboard-payment-overview/`)
      .then((res) => {
        setPaymentsOverview(res.data);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <div className={classes.box}>
        <Typography style={{ marginBottom: "20px", fontSize: "22px" }}>
          Shipments Overview
        </Typography>
        <Grid container spacing={3}>
          <Grid item lg={3} md={6} xs={12} className={classes.griditem}>
            <Button
              className={classes.paper}
              style={{ background: "purple", color: "white" }}
              startIcon={
                <PublishIcon
                  style={{
                    fontSize: "50px",
                    marginTop: "-15px",
                    marginRight: "30px",
                  }}
                />
              }
            >
              Requested <br />
              {shipmentOverview.requested}
            </Button>
          </Grid>
          <Grid item lg={3} md={6} xs={12} className={classes.griditem}>
            <Button
              startIcon={
                <RotateLeftIcon
                  style={{
                    fontSize: "50px",
                    marginTop: "-15px",
                    marginRight: "30px",
                  }}
                />
              }
              className={classes.paper}
              style={{ background: "#6C97C9", color: "white" }}
            >
              Processing <br />
              {shipmentOverview.processing}
            </Button>
          </Grid>
          <Grid item lg={3} md={6} xs={12} className={classes.griditem}>
            <Button
              startIcon={
                <AirportShuttleIcon
                  style={{
                    fontSize: "50px",
                    marginTop: "-15px",
                    marginRight: "30px",
                  }}
                />
              }
              className={classes.paper}
              style={{ background: "#6C97C9", color: "white" }}
            >
              Dispatched <br />
              {shipmentOverview.dispatched}
            </Button>
          </Grid>
          <Grid item lg={3} md={6} xs={12} className={classes.griditem}>
            <Button
              startIcon={
                <CancelIcon
                  style={{
                    fontSize: "50px",
                    marginTop: "-15px",
                    marginRight: "30px",
                  }}
                />
              }
              className={classes.paper}
              style={{ background: "#ED3E3E", color: "white" }}
            >
              Rejected <br />
              {shipmentOverview.rejected}
            </Button>
          </Grid>
          <Grid item lg={3} md={6} xs={12} className={classes.griditem}>
            <Button
              startIcon={
                <DoneIcon
                  style={{
                    fontSize: "50px",
                    marginTop: "-15px",
                    marginRight: "30px",
                  }}
                />
              }
              className={classes.paper}
              style={{ background: "#58BC34", color: "white" }}
            >
              Delivered <br />
              {shipmentOverview.delivered}
            </Button>
          </Grid>
          <Grid item lg={3} md={6} xs={12} className={classes.griditem}>
            <Button
              startIcon={
                <KeyboardReturnIcon
                  style={{
                    fontSize: "50px",
                    marginTop: "-15px",
                    marginRight: "30px",
                  }}
                />
              }
              className={classes.paper}
              style={{ background: "#ED3E3E", color: "white" }}
            >
              Returned <br />
              {shipmentOverview.returned}
            </Button>
          </Grid>
          <Grid item lg={3} md={6} xs={12} className={classes.griditem}>
            <Button
              startIcon={
                <DeleteForeverIcon
                  style={{
                    fontSize: "50px",
                    marginTop: "-15px",
                    marginRight: "30px",
                  }}
                />
              }
              className={classes.paper}
              style={{ background: "#ED3E3E", color: "white" }}
            >
              Canceled <br />
              {shipmentOverview.cancel}
            </Button>
          </Grid>
          <Grid item lg={3} md={6} xs={12} className={classes.griditem}>
            <Button
              startIcon={
                <FormatListBulletedIcon
                  style={{
                    fontSize: "50px",
                    marginTop: "-15px",
                    marginRight: "30px",
                  }}
                />
              }
              className={classes.paper}
              style={{ background: "orange", color: "white" }}
            >
              All Shipments <br />
              {shipmentOverview.all_shipments}
            </Button>
          </Grid>
        </Grid>
      </div>

      <Grid container spacing={2} className={classes.tablecontainer}>
        <Grid item md={7} xs={12}>
          <TableContainer component={Paper} style={{display:"flex",justifyContent:"space-between" ,paddingTop: "13px", paddingBottom:"13px" }}>
          <div style={{display:"flex"}}>
            <Typography style={{ marginRight:"6px", fontSize:"18px",marginLeft:"12px"}} >New Orders</Typography>
            <small>Per day</small>
          </div>
          <Typography style={{ marginRight:"12px", fontSize:"18px"}}>Last 7 days</Typography>
          </TableContainer>
        </Grid>

        <Grid item lg md={5} xs={12} className={classes.griditem}>
          <TableContainer component={Paper}>
            <Typography
              style={{
                height: "150px",
                color: "red",
                marginTop: "10px",
                marginLeft: "12px",
                fontWeight:"bold"
              }}
            >
              Latest Remarks on Shipments (Last 10)
            </Typography>
          </TableContainer>
        </Grid>
      </Grid>

      <div className={classes.box}>
        <Typography style={{ marginBottom: "20px", fontSize: "22px" }}>
          Payments Overview
        </Typography>
        <Grid container spacing={3}>
          <Grid item lg={4} md={6} xs={12} className={classes.griditem}>
            <Paper
              className={classes.paper}
              style={{
                color: "black",
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <div>
                Current Balance <br /> Rs. {paymentsOverview.current_balance}/-
              </div>
              <div>
                <AccountBalanceWalletIcon
                  style={{ fontSize: "50px", color: "green" }}
                />
              </div>
            </Paper>
          </Grid>
          <Grid item lg={4} md={6} xs={12} className={classes.griditem}>
            <Paper
              className={classes.paper}
              style={{
                color: "black",
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <div>
                Incomplete COD Amount <br /> Rs.{" "}
                {paymentsOverview.incomplete_cod_amount}/-
              </div>
              <div>
                <MoneyIcon style={{ fontSize: "50px", color: "orange" }} />
              </div>
            </Paper>
          </Grid>
          <Grid item lg={4} md={6} xs={12} className={classes.griditem}>
            <Paper
              className={classes.paper}
              style={{
                color: "black",
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <div>
                Incomplete Charge <br /> Rs.{" "}
                {paymentsOverview.incomplete_charge}/-
              </div>
              <div>
                <MoneyIcon style={{ fontSize: "50px", color: "purple" }} />
              </div>
            </Paper>
          </Grid>
          <Grid item lg={4} md={6} xs={12} className={classes.griditem}>
            <Paper
              className={classes.paper}
              style={{
                color: "black",
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <div>
                Total COD Value <br /> Rs. {paymentsOverview.total_cod_received}
                /-
              </div>
              <div>
                <MonetizationOnIcon
                  style={{ fontSize: "50px", color: "purple" }}
                />
              </div>
            </Paper>
          </Grid>
          <Grid item lg={4} md={6} xs={12} className={classes.griditem}>
            <Paper
              className={classes.paper}
              style={{
                color: "black",
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <div>
                Total Charge Paid <br /> Rs.{" "}
                {paymentsOverview.total_charge_paid}/-
              </div>
              <div>
                <MonetizationOnIcon
                  style={{ fontSize: "50px", color: "red" }}
                />
              </div>
            </Paper>
          </Grid>
          <Grid item lg={4} md={6} xs={12} className={classes.griditem}>
            <Paper
              className={classes.paper}
              style={{
                color: "black",
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <div>
                Net Amount Received <br /> Rs.{" "}
                {paymentsOverview.net_paid_or_received}/-
              </div>
              <div>
                <MonetizationOnIcon
                  style={{ fontSize: "50px", color: "gray" }}
                />
              </div>
            </Paper>
          </Grid>
        </Grid>
      </div>
    </main>
  );
};

export default Dashboard;
