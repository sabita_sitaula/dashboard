import React, { useState, useContext, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useSearchParams } from "react-router-dom";
import LockIcon from "@material-ui/icons/Lock";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useNavigate } from "react-router-dom";
import { authContext } from "../../App";
import {
  Button,
  TextField,
  Typography,
  FormGroup,
  InputAdornment,
  Grid,
  Paper,
} from "@material-ui/core";

const useStyles = makeStyles({
  login: {
    position: "relative",
    padding: "30px",
  },
  loadingImg: {
    position: "absolute",
    marginLeft: "auto",
    marginRight: "auto",
    top: 95,
    left: 0,
    right: 0,
    textAlign: "center",
  },
  pwForm: {
    margin: "87px auto",
    width: 350,
    height: "60vh",
    padding: " 20px 20px 0",
  },
  pwTitle: {
    margin: "10px 0px",
  },
  forgotBtn: {
    marginTop: "20px",
  },
});

const ResetPassword = () => {
  const context = useContext(authContext);
  const [isLoading, setIsLoading] = useState(false);
  const [showErr, setShowErr] = useState(false);
  const [disable, setDisable] = useState(false);
  let navigate = useNavigate();
  const [qparams] = useSearchParams();
  const token = qparams.getAll("t")[0];
  const uid = qparams.getAll("u")[0];
  const password_reset = "api/v1/customer/reset-password";
  const classes = useStyles();
  const [password, setPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const instance = context.user.axiosInstance;

  const handleSubmit = (e) => {
    setIsLoading(true);
    e.preventDefault();
    instance
      .post(`/${password_reset}/${uid}/${token}/`, {
        new_password1: password,
        new_password2: newPassword,
      })
      .then((res) => {
        setIsLoading(false);
        if (res.data.status === "success") {
          navigate("/");
        }
      })
      .catch((err) => {
        const errPw = err.response.data.non_field_errors[0];
        if (errPw === "nomatch") {
          setShowErr(true);
          setTimeout(() => {
            setShowErr(false)
          },2000)
          setDisable(false);
        }
        setIsLoading(false);
      });
    setDisable(true);
  };

  useEffect(() => {
    document.title = "title";
    instance
      .get(`/${password_reset}/${uid}/${token}/`)
      .then((res) => {
        if (res.data.status === "failure") {
          navigate("/forgot-password");
        }
      })
      .catch((err) => {
        navigate("/forgot-password");
      });
  }, []);

  return (
    <Grid container justifyContent="center" spacing={3}>
      <Grid item sm={4} md={4} lg={4}>
        <Paper className={classes.login}>
          {isLoading && <CircularProgress className={classes.loadingImg} />}
          <Typography
            className={classes.pwTitle}
            align="center"
            variant="h6"
            display="initial"
          >
            Create New Password
          </Typography>
          <form onSubmit={handleSubmit}>
            <FormGroup>
              <TextField
                type="password"
                label="Password"
                variant="outlined"
                margin="normal"
                value={password}
                disabled={disable}
                onChange={(e) => setPassword(e.target.value)}
                required
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <LockIcon />
                    </InputAdornment>
                  ),
                }}
              />
              <TextField
                type="password"
                disabled={disable}
                label="Confirm Password"
                variant="outlined"
                margin="normal"
                value={newPassword}
                onChange={(e) => setNewPassword(e.target.value)}
                required
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <LockIcon />
                    </InputAdornment>
                  ),
                }}
              />
              <Button
                className={classes.forgotBtn}
                disabled={disable}
                variant="contained"
                color="primary"
                type="submit"
              >
                Submit
              </Button>
            </FormGroup>
          </form>
          {showErr && <small style={{color:"red", marginTop:"10px",display:"inline-block",textTransform:"capitalize"}}>Password didn't match!!</small>}
        </Paper>
      </Grid>
    </Grid>
  );
};

export default ResetPassword;
