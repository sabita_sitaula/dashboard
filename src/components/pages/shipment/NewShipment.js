import React, { useEffect, useContext, useState } from "react";
import {
  makeStyles,
  Paper,
  Typography,
  TextField,
  Button,
  Divider,
  MenuItem,
  Grid,
} from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { authContext } from "../../../App";
import { useNavigate } from "react-router-dom";
import { Alert } from "@material-ui/lab";

import Snackbar from "@material-ui/core/Snackbar";

const useStyles = makeStyles((theme) => ({
  toolbar: {
    minHeight: "56px",
    maxHeight: "56px",
    paddingTop: "15px",
  },
  content: {
    marginLeft: "240px",
    flexGrow: 1,
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      marginLeft: "0px",
    },
  },
  paper: {
    paddingLeft: "25px",
    paddingRight: "25px",
  },
  typo: {
    marginLeft: "25px",
    marginTop: "10px",
    fontSize: "19px",
    paddingTop: "15px",
  },
  form: {
    marginTop: "25px",
  },
  note: {
    display: "flex",
    marginTop: "15px",
  },
  container: {
    display: "flex",
    justifyContent: "space-between",
  },
  formControl: {
    width: "100%",
  },
  control: {
    width: "50%",
    [theme.breakpoints.down("xs")]: {
      width: "100%",
    },
  },
  button: {
    color: "white",
    marginTop: "25px",
    marginBottom: "20px",
    textAlign: "center",
  },
}));

const NewShipment = ({ title }) => {
  let navigate = useNavigate();
  const classes = useStyles();
  const context = useContext(authContext);
  const [city, setCity] = useState([]);
  const [charges, setCharge] = useState([]);
  const [newshipment, setNewShipment] = useState({
    receiver_name: "",
    receiver_city: "",
    receiver_address: "",
    receiver_contact_number: "",
    cod_value: 0,
    parcel_type: "",
    shipping_zone: "",
    receiver_alt_contact_number: "",
  });

  const [open, setOpen] = React.useState(false);
  const instance = context.user.axiosInstance;

  useEffect(() => {
    document.title = title;
    instance
      .get(`/api/v1/customer/shipment/city-list/`)
      .then((res) => {
        setCity(res.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const handleCityChange = (e, city) => {
    if (city) {
      var city_id = city.id;
      instance
        .get(
          `/api/v1/customer/shipment-${city_id}/shipment-zones/`
        )
        .then((res) => {
          setCharge(res.data.data);
          setNewShipment({ ...newshipment, receiver_city: city.id });
        })
        .catch((err) => console.log(err));
    } else {
      setCharge([]);
    }
  };
  const onInputChange = (e) => {
    setNewShipment({ ...newshipment, [e.target.name]: e.target.value });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    // post to server if all data are avaialable
    if (
      newshipment.receiver_name &&
      newshipment.receiver_city &&
      newshipment.cod_value &&
      newshipment.shipping_zone &&
      newshipment.receiver_contact_number &&
      newshipment.parcel_type &&
      newshipment.receiver_address
    ) {
      instance
        .post(
          `/api/v1/customer/place-new-shipment/`,
          newshipment
        )
        .then((res) => {
          console.log(res.data);
          if (res.data.status === "success") {
            setOpen(true);
            setTimeout(function () {
              navigate("/shipments");
            }, 1000);
          }
        })
        .catch((err) => console.log(err));
    }
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <Paper className={classes.paper}>
        <Typography variant="h6" display="initial" className={classes.typo}>
          REQUEST NEW SHIPMENT TO DELIVER
        </Typography>

        <small
          style={{
            display: "inline-block",
            color: "red",
            marginLeft: "25px",
            marginBottom: "35px",
          }}
        >
          Fields marked with '*' are required fields
        </small>

        <Divider />
        <form className={classes.form} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                label="Receiver Full Name:"
                value={newshipment.receiver_name}
                onChange={(e) => onInputChange(e)}
                name="receiver_name"
                variant="outlined"
                autoFocus
                margin="normal"
                required
                className={classes.formControl}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                id="select"
                value={newshipment.parcel_type}
                onChange={(e) => onInputChange(e)}
                name="parcel_type"
                label="Parcel Type:"
                variant="outlined"
                margin="normal"
                select
                required
                className={classes.formControl}
              >
                <MenuItem value="clothing">Clothing</MenuItem>
                <MenuItem value="books">Books</MenuItem>
                <MenuItem value="electronics">Electronics</MenuItem>
                <MenuItem value="documents">documents</MenuItem>
                <MenuItem value="foods">Foods</MenuItem>
                <MenuItem value="fragile">Fragile</MenuItem>
                <MenuItem value="others">Others</MenuItem>
              </TextField>
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                label="Receiver Contact Number:"
                type="number"
                value={newshipment.receiver_contact_number}
                onChange={(e) => onInputChange(e)}
                name="receiver_contact_number"
                variant="outlined"
                margin="normal"
                className={classes.formControl}
                required
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                label="Receiver Alternative Contact Number:"
                value={newshipment.receiver_alt_contact_number}
                onChange={(e) => onInputChange(e)}
                name="receiver_alt_contact_number"
                variant="outlined"
                margin="normal"
                className={classes.formControl}
                required
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                label="Receiver Street Address:"
                value={newshipment.receiver_address}
                onChange={(e) => onInputChange(e)}
                name="receiver_address"
                variant="outlined"
                margin="normal"
                className={classes.formControl}
                required
              />
            </Grid>

            <Grid item xs={12} sm={6}>
              <Autocomplete
                id="combo-box-demo"
                onChange={handleCityChange}
                options={city}
                getOptionLabel={(data) => `${data.name},(${data.district})`}
                renderInput={(params) => (
                  <TextField
                    style={{ marginTop: "15px" }}
                    {...params}
                    label="Receiver City"
                    variant="outlined"
                    value={newshipment.receiver_city}
                    // onChange={(e)=>setNewShipment(e.target.value)}
                    name="receiver_city"
                    required
                  />
                )}
              />
            </Grid>

            <Grid item xs={12} sm={6}>
              <TextField
                id="select"
                onChange={onInputChange}
                label="Service Provider:"
                variant="outlined"
                margin="normal"
                value={newshipment.shipping_zone}
                name="shipping_zone"
                select
                required
                className={classes.formControl}
              >
                {charges.map((data) => (
                  <MenuItem value={data.id} key={"city_" + data.id}>
                    {data.logistics_branch} ({data.shipping_charge_per_kg})
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
          </Grid>

          <TextField
            label="COD Value To Receive:"
            placeholder="0"
            type="number"
            variant="outlined"
            margin="normal"
            value={newshipment.cod_value}
            onChange={onInputChange}
            name="cod_value"
            required
            className={classes.control}
          />
          <div className={classes.note}>
            <big>NOTE: </big>
            <small style={{ marginTop: "5px", marginLeft: "3px" }}>
              Fill amount you need to collect from your customer in order to
              deliver this order
            </small>
          </div>
          <Button
            variant="contained"
            style={{ background: "#319F8D", color: "white" }}
            className={classes.button}
            type="submit"
          >
            PLACE ORDER
          </Button>
          {open && (
            <>
              <Snackbar open={open} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success">
                  Form has been submitted Successfully!
                </Alert>
              </Snackbar>
            </>
          )}
        </form>
      </Paper>
    </main>
  );
};

export default NewShipment;
