import React, { useEffect, useState, useContext } from "react";
import { authContext } from "../../../App";
import {
  Divider,
  makeStyles,
  Typography,
  List,
  ListItem,
  ListItemText,
  Grid,
  Button
} from "@material-ui/core";
import { TableContainer, Paper, TextField } from "@material-ui/core";
import { useParams } from "react-router";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";

const useStyles = makeStyles((theme) => ({
  toolbar: {
    minHeight: "56px",
    maxHeight: "56px",
    paddingTop: "15px",
  },
  content: {
    marginLeft: "240px",
    flexGrow: 1,
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      marginLeft: "0px",
    },
  },
  tablecontainer: {
    display: "flex",
    justifyContent: "space-between",
  },
}));

const ShipmentDetails = ({title}) => {
  const [shipmentDetails, setShipmentDetails] = useState({});
  const context = useContext(authContext);
  const { tracking_code } = useParams();
  const instance = context.user.axiosInstance;

  useEffect(() => {
    document.title = title;
    instance
      .get(
        `/api/v1/customer/shipment-${tracking_code}/detail/`
      )
      .then((res) => {
        console.log(res.data);
        setShipmentDetails(res.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const classes = useStyles();

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <Grid container spacing={2} className={classes.tablecontainer}>
        <Grid item md={8} xs={12}>
          <TableContainer component={Paper}>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <Typography
                style={{ paddingTop: "13px" }}
              >
                Shipment Information: {tracking_code}
              </Typography>
              <TextField
                style={{ marginLeft: "6px" }}
                inputProps={{
                  style: {
                    padding: "9.5px 14px",
                  },
                }}
                id="outlined-basic"
                placeholder="type keyword & enter"
                variant="outlined"
              />
            </div>
            <Divider />
            <List component="nav" aria-label="main mailbox folders">
              <ListItem>
                <Grid container lg="auto" spacing={2}>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Receiver Name"
                      secondary={shipmentDetails.receiver_name}
                    />
                  </Grid>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Receiver Address"
                      secondary={shipmentDetails.receiver_address}
                    />
                  </Grid>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Receiver City"
                      secondary={shipmentDetails.receiver_city}
                    />
                  </Grid>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Receiver Contact Number"
                      secondary={shipmentDetails.receiver_contact_number}
                    />
                  </Grid>
                </Grid>
              </ListItem>
            </List>
            <Divider />
            <List component="nav">
              <ListItem>
                <Grid container lg="auto" spacing={2}>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Cod Info"
                      secondary={shipmentDetails.cod_info}
                    />
                  </Grid>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Shipping Charge"
                      secondary={shipmentDetails.shipping_charge}
                    />
                  </Grid>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Cod Handling Charge"
                      secondary={shipmentDetails.cod_handling_charge}
                    />
                  </Grid>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Rejection Handling Charge"
                      secondary={shipmentDetails.rejection_handling_charge}
                    />
                  </Grid>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Service Payment Status"
                      secondary={
                        shipmentDetails.service_payment_status ? (
                          <span style={{ color: "green" }}>paid</span>
                        ) : (
                          <span style={{ color: "red" }}>Not Paid </span>
                        )
                      }
                    />
                  </Grid>
                </Grid>
              </ListItem>
            </List>
            <Divider />
            <List component="nav">
              <ListItem>
                <Grid container lg="auto" spacing={2}>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Shipment Origin"
                      secondary={shipmentDetails.shipment_origin}
                    />
                  </Grid>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Tracking Code"
                      secondary={
                        <span style={{ color: "#58BC34" }}>
                          {shipmentDetails.tracking_code}
                        </span>
                      }
                    />
                  </Grid>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Created At"
                      secondary={shipmentDetails.created_at}
                    />
                  </Grid>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Reference Code"
                      secondary={
                        shipmentDetails.reference_code
                          ? shipmentDetails.reference_code
                          : "none"
                      }
                    />
                  </Grid>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Parcel Type"
                      secondary={shipmentDetails.parcel_type}
                    />
                  </Grid>
                </Grid>
              </ListItem>
            </List>
            <Divider />
            <List component="nav">
              <ListItem>
                <Grid container lg="auto" spacing={2}>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Weight"
                      secondary={
                        shipmentDetails.weight ? shipmentDetails.weight : "none"
                      }
                    />
                  </Grid>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Weight Unit"
                      secondary={shipmentDetails.weight_unit}
                    />
                  </Grid>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Length"
                      secondary={
                        shipmentDetails.length ? shipmentDetails.length : "none"
                      }
                    />
                  </Grid>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Breadth"
                      secondary={
                        shipmentDetails.breadth
                          ? shipmentDetails.breadth
                          : "none"
                      }
                    />
                  </Grid>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Height"
                      secondary={
                        shipmentDetails.height ? shipmentDetails.height : "none"
                      }
                    />
                  </Grid>
                  <Grid item md={4}>
                    <ListItemText
                      primary="Length Unit"
                      secondary={shipmentDetails.length_unit}
                    />
                  </Grid>
                </Grid>
              </ListItem>
            </List>
          </TableContainer>
        </Grid>

        <Grid item lg md={4} xs={12} className={classes.griditem}>
          <TableContainer component={Paper}>
            <Typography style={{ height:"60px", display:"flex", alignItems:"center", justifyContent:"center" }}>
              Remarks on this Shipment
            </Typography>
            <Divider />
            <div style={{ height: "290px" }}></div>
            <Grid container>
              <Grid item xs={12}>
                <TextareaAutosize
                  autoFocus
              aria-label="minimum height"
              minRows={5}
                  placeholder="Write your Remarks"
                  style={{marginLeft:"40px", width:"250px"}}
            />
              </Grid>
              <Grid item xs={12}>
            <Button variant="contained" style={{marginLeft:"40px", marginBottom:"20px", marginTop:"10px", background:"#319F8D", color:"white"}}>
              Add Remarks
            </Button>
            </Grid>
            </Grid>
          </TableContainer>
        </Grid>
      </Grid>
    </main>
  );
};

export default ShipmentDetails;
