import React, { useEffect, useState, useContext } from "react";
import { authContext } from "../../../App";
import { Button, makeStyles, TablePagination } from "@material-ui/core";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
} from "@material-ui/core";
import { useNavigate } from "react-router";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles((theme) => ({
  toolbar: {
    minHeight: "56px",
    maxHeight: "56px",
    paddingTop: "15px",
  },
  container: {
    maxHeight: 440,
  },
  content: {
    marginLeft: "240px",
    flexGrow: 1,
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      marginLeft: "0px",
    },
  },
  table: {
    minWidth: 650,
  },
  tablecell: {
    fontWeight: "bold",
  },
  box: {
    flexGrow: 1,
    marginBottom: "30px",
    marginTop: "20px",
    borderColor: "black",
  },
  griditem: {
    height: "140px",
    width: "50px",
  },

  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    height: "100px",
    width: "135px",
    fontSize: "17px",
    backgroundColor: "#319f77",
    color: "white",
    fontSize: "17px",
    textTransform: "capitalize",
    lineHeight: "25px",
    paddingTop: "4px"
  },
  typo: {
    fontSize: "18px",
    marginLeft: "15px",
    marginTop: "20px",
    marginBottom: "20px",
  },
  loading: {
    position: "absolute",
    marginLeft: "auto",
    marginRight:"auto",
    left: 0,
    right: 0,
    top: 600,
    zIndex:10,
    [theme.breakpoints.down("lg")]: {
      top: 280,
    },
    [theme.breakpoints.down("md")]: {
      top: 540,
      left:280
    },
    [theme.breakpoints.down("sm")]: {
      left:30
    },
  }

}));

const RequestPayment = ({title}) => {
  const [RequestPayment, setRequestPayment] = useState([]);
  const context = useContext(authContext);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const instance = context.user.axiosInstance;

  useEffect(() => {
    setIsLoading(true);
    document.title = title;
    instance
      .get(`/api/v1/customer/request-payment-list/`)
      .then((res) => {
        setIsLoading(false);
        setRequestPayment(res.data.results.data);
        
      })
      .catch((err) => console.log(err));
  }, []);

  const classes = useStyles();

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleClick = () => {
    navigate("/customer/payment-request-create");
  }

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />

      <div className={classes.box}>
        <Grid container spacing={3}>
          <Grid item lg md={4} xs={6} className={classes.griditem}>
            <Button
              className={classes.paper}
              style={{ background: "#ED3E3E", color: "white",paddingTop:"1px" }}
            >
              Unsettled Requests
            </Button>
          </Grid>
          <Grid item lg md={4} xs={6} className={classes.griditem}>
            <Button
              className={classes.paper}
              style={{ background: "#58BC34", color: "white" }}
            >
              Total Settled Requests
            </Button>
          </Grid>
          <Grid item lg md={4} xs={6} className={classes.griditem}>
            <Button
              className={classes.paper}
              style={{ background: "#58BC34", color: "white", paddingTop:"1px" }}
            >
              Overall requested
            </Button>
          </Grid>
          <Grid item lg md={4} xs={6} className={classes.griditem}>
            <Paper
              className={classes.paper}
              style={{ background: "#58BC34", color: "white", paddingTop:"14px" }}
            >
              Money in Wallet
            </Paper>
          </Grid>
          <Grid item lg md={4} xs={6} className={classes.griditem}>
            <Button
              style={{
                background: "#319F8D",
                color: "white",
                fontSize: "15px",
                marginTop: "18px",
              }}
              onClick={handleClick}
            >
              Request My Payment
            </Button>
          </Grid>
        </Grid>
      </div>

      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell className={classes.tablecell}>
                Service&nbsp;Provider
              </TableCell>
              <TableCell align="left" className={classes.tablecell}>
                Request&nbsp;Amount
              </TableCell>
              <TableCell align="left" className={classes.tablecell}>
                Preferred&nbsp;Account
              </TableCell>
              <TableCell align="left" className={classes.tablecell}>
                Requested&nbsp;Date
              </TableCell>
              <TableCell align="left" className={classes.tablecell}>
                Payment&nbsp;Status
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {isLoading && <CircularProgress className={classes.loading} />}
            {RequestPayment.slice(
              page * rowsPerPage,
              page * rowsPerPage + rowsPerPage
            ).map((request) => {
              return (
                <TableRow hover>
                  <TableCell>{request.logistics_branch}</TableCell>
                  <TableCell align="left">{request.request_amount}</TableCell>
                  <TableCell align="left">
                    {request.preferred_receiving_account}
                  </TableCell>
                  <TableCell align="left">{request.requested_date}</TableCell>
                  <TableCell align="left">{request.is_settled}</TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10]}
        component="div"
        count={RequestPayment.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </main>
  );
};

export default RequestPayment;
