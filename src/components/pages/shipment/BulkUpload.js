import React from "react";
import { makeStyles } from "@material-ui/core";
const useStyles = makeStyles((theme) => ({
           
    // necessary for content to be below app bar
    toolbar: {
      minHeight: "56px",
      maxHeight: "56px",
      paddingTop: "15px",
    },
    content: {
        marginLeft:"480px",
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }));
const NewShipment = () => {
    const classes = useStyles();
    
  return (
          <main className={classes.content}>    
              <div className={classes.toolbar} />
              <h3>Payment List</h3>
            </main>
  );
};

export default NewShipment;
