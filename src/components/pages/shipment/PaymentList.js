import React, { useEffect, useState, useContext } from "react";
import { authContext } from "../../../App";
import { Divider, makeStyles } from "@material-ui/core";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  TablePagination,
  Typography,
  Button
} from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useNavigate } from "react-router";

const useStyles = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
  toolbar: {
    minHeight: "56px",
    maxHeight: "56px",
    paddingTop: "15px",
  },
  content: {
    marginLeft: "240px",
    flexGrow: 1,
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      marginLeft: "0px",
    },
  },
  table: {
    minWidth: 650,
  },
  tablecell: {
    fontWeight: "bold",
  },
  typo: {
    fontSize: "18px",
    marginLeft: "15px",
    marginTop: "20px",
    marginBottom: "30px",
  },
  loading: {
    position: "absolute",
    margin: "auto",
    left: 0,
    right: 0,
    top: 200,
    [theme.breakpoints.down("md")]: {
      top: 170,
      left:140
    },
    [theme.breakpoints.down("xs")]: {
      right:100
    },
  },
}));

const PaymentList = ({title}) => {
  const [paymentList, setPaymentList] = useState([]);
  const context = useContext(authContext);
  const [isLoading, setIsLoading] = useState(false);
  const instance = context.user.axiosInstance;
  const navigate = useNavigate();

  useEffect(() => {
    setIsLoading(true);
    document.title = title;
    instance
      .get(`/api/v1/customer/payment-list/`)
      .then((res) => {
        setIsLoading(false);
        setPaymentList(res.data.results.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleButtonClick = (id) => {
    navigate(`/customer/payment-${id}-detail`);
  };

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <TableContainer component={Paper}>
        <Typography className={classes.typo}>
          All Received Payments (Invoices)
        </Typography>
        <Divider />
        <Table className={classes.container} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell className={classes.tablecell}>
                Payment&nbsp;Request
              </TableCell>
              <TableCell className={classes.tablecell} align="left">
                Payment&nbsp;Method
              </TableCell>
              <TableCell className={classes.tablecell} align="left">
                Logistic&nbsp;Branch
              </TableCell>
              <TableCell className={classes.tablecell} align="left">
                Net&nbsp;paid
              </TableCell>
              <TableCell className={classes.tablecell} align="left">
                Order&nbsp;Date
              </TableCell>
              <TableCell className={classes.tablecell} align="left">
                Action
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {isLoading && <CircularProgress className={classes.loading} />}
            {paymentList
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((payment) => (
                <TableRow hover key={payment.created_at}>
                  <TableCell>{payment.payment_request}</TableCell>
                  <TableCell align="left">{payment.payment_method}</TableCell>
                  <TableCell align="left">{payment.logistics_branch}</TableCell>
                  <TableCell align="left">
                    {payment.net_paid_or_received}
                  </TableCell>
                  <TableCell align="left">{payment.created_at}</TableCell>
                  <TableCell align="left">
                    <Button
                      style={{
                        background: "#319F8D",
                        color: "white",
                        padding: "5px",
                        fontSize: "11px",
                      }}
                      onClick={() => handleButtonClick(payment.id)}
                    >
                      View
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10]}
        component="div"
        count={paymentList.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </main>
  );
};

export default PaymentList;
