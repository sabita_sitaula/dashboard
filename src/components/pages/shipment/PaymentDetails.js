import React, { useEffect, useState, useContext } from "react";
import { authContext } from "../../../App";
import { Divider, makeStyles } from "@material-ui/core";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  TablePagination,
  Typography,
  Button,
  Grid,
} from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useNavigate } from "react-router";
import { useParams } from "react-router";

const useStyles = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
  toolbar: {
    minHeight: "56px",
    maxHeight: "56px",
    paddingTop: "15px",
  },
  content: {
    marginLeft: "240px",
    flexGrow: 1,
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      marginLeft: "0px",
    },
  },
  table: {
    minWidth: 650,
  },
  tablecell: {
    fontWeight: "bold",
  },
  typo: {
    fontSize: "18px",
    marginLeft: "15px",
    marginTop: "20px",
    marginBottom: "30px",
  },
  loading: {
    position: "absolute",
    margin: "auto",
    left: 0,
    right: 0,
    top: 200,
    [theme.breakpoints.down("md")]: {
      top: 170,
      left: 140,
    },
    [theme.breakpoints.down("xs")]: {
      right: 100,
    },
  },
  box: {
    flexGrow: 1,
    marginBottom: "30px",
    marginTop: "20px",
    borderColor: "black",
  },
  griditem: {
    height: "140px",
    width: "50px",
  },

  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    height: "110px",
    width: "135px",
    fontSize: "15px",
    backgroundColor: "#7C7C7C",
    color: "white",
  },
}));

const PaymentDetails = ({ title }) => {
  const [PaymentDetails, setPaymentDetails] = useState([]);
  const [otherDetails, setOtherDetails] = useState([]);
  const context = useContext(authContext);
  const [isLoading, setIsLoading] = useState(false);
  const instance = context.user.axiosInstance;
  const navigate = useNavigate();
  const { id } = useParams();

  useEffect(() => {
    setIsLoading(true);
    document.title = title;
    instance
      .get(`/api/v1/customer/payment-request-${id}/detail/`)
      .then((res) => {
        setIsLoading(false);
        console.log(res.data);
        setPaymentDetails(res.data.data.shipments);
        setOtherDetails(res.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />

      <div className={classes.box}>
        <Grid container spacing={3}>
          <Grid item lg md={4} xs={6} className={classes.griditem}>
            <Paper className={classes.paper}>
              Payment Made By
              <Divider />
              <small>{otherDetails.logistics_branch}</small>
            </Paper>
          </Grid>
          <Grid item lg md={4} xs={6} className={classes.griditem}>
            <Paper className={classes.paper} style={{ fontSize: "17px" }}>
              Payment Date
              <Divider />
              <small>{otherDetails.payment_date}</small>
            </Paper>
          </Grid>
          <Grid item lg md={4} xs={6} className={classes.griditem}>
            <Paper className={classes.paper} style={{ fontSize: "17px" }}>
              Requested On
              <Divider />
              <small>{otherDetails.payment_request}</small>
            </Paper>
          </Grid>
          <Grid item lg md={4} xs={6} className={classes.griditem}>
            <Paper className={classes.paper} style={{ fontSize: "12px" }}>
              <span style={{ fontSize: "14px" }}>Receiver Account</span>
              <Divider />
              <small>{otherDetails.payment_method}</small>
            </Paper>
          </Grid>
          <Grid item lg md={4} xs={6} className={classes.griditem}>
            <Paper
              className={classes.paper}
              style={{
                fontSize:"17px"
              }}
            >
              Paid/Received Amount
              <Divider />
              <small>{otherDetails.net_paid_or_received}</small>
            </Paper>
          </Grid>
          <Grid item lg md={4} xs={6} className={classes.griditem}>
            <Paper className={classes.paper} style={{ fontSize: "17px" }}>
              Uploaded Slip
              <Divider />
              <small>{otherDetails.slip}</small>
            </Paper>
          </Grid>
        </Grid>
      </div>

      <TableContainer component={Paper}>
        <Typography className={classes.typo}>
          Included Shipments (Completed)
        </Typography>
        <Divider />
        <Table className={classes.container} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell className={classes.tablecell}>Shipment</TableCell>
              {/* <TableCell className={classes.tablecell} align="left">
                Customer
              </TableCell> */}
              <TableCell className={classes.tablecell} align="left">
                Shipment Status
              </TableCell>
              <TableCell className={classes.tablecell} align="left">
                COD
              </TableCell>
              <TableCell className={classes.tablecell} align="left">
                Charge
              </TableCell>
              <TableCell className={classes.tablecell} align="left">
                Net&nbsp;Amount
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {isLoading && <CircularProgress className={classes.loading} />}
            {PaymentDetails.slice(
              page * rowsPerPage,
              page * rowsPerPage + rowsPerPage
            ).map((payment) => (
              <TableRow hover key={payment.id}>
                <TableCell>{payment.tracking_code}</TableCell>
                <TableCell align="left">
                  <span style={{ background: "#16D39A", color: "white" }}>
                    {payment.shipment_status}
                  </span>
                </TableCell>
                <TableCell align="left">{payment.cod_value}</TableCell>
                <TableCell align="left">{payment.service_charge}</TableCell>
                <TableCell align="left">{payment.net_amount}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10]}
        component="div"
        count={PaymentDetails.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </main>
  );
};

export default PaymentDetails;
