import React, { useEffect, useState, useContext } from "react";
import { authContext } from "../../../App";
import {
  Button,
  Divider,
  makeStyles,
  Typography,
} from "@material-ui/core";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  TablePagination,
  Grid,
  TextField,
} from "@material-ui/core";
import { useNavigate } from "react-router";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
  toolbar: {
    minHeight: "56px",
    maxHeight: "56px",
    paddingTop: "15px",
  },
  content: {
    marginLeft: "240px",
    flexGrow: 1,
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      marginLeft: "0px",
    },
  },
  table: {
    minWidth: 650,
  },
  tablecell: {
    fontWeight: "bold",
  },
  box: {
    flexGrow: 1,
    marginBottom: "30px",
    marginTop: "20px",
    borderColor: "black",
  },
  griditem: {
    height: "140px",
    width: "40px",
  },

  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    height: "100px",
    width: "130px",
    fontSize: "20px",
    color: "white",
    fontSize: "19px",
    textTransform: "capitalize",
    lineHeight: "25px",
    paddingTop: "2px"
  },
  typo: {
    fontSize: "18px",
    marginLeft: "15px",
    marginTop: "20px",
    marginBottom: "30px",
  },
  loading: {
    position: "absolute",
    top: 750,
    marginLeft: "auto",
    marginRight: "auto",
    left: 0,
    right: 0,
    textAlign: "center",
    [theme.breakpoints.down("lg")]: {
      top: 280,
    },
    [theme.breakpoints.down("md")]: {
      top: 540,
      left: 140,
    },
    [theme.breakpoints.down("sm")]: {
      right: 140,
    },
    
  },
}));



const ShipmentList = (props) => {
  const [shipmentList, setShipmentList] = useState([]);
  // const [otherLists, setOtherLists] = useState([]);
  const context = useContext(authContext);
  const { title } = props;
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const instance = context.user.axiosInstance;

  useEffect(() => {
    setIsLoading(true);
    document.title = title;
    instance
      .get(`/api/v1/customer/shipment-list/`)
      .then((resp) => {
        setIsLoading(false);
        setShipmentList(resp.data.results.data);
      });
  }, []);

  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleButtonClick = (tracking_code) => {
    navigate(`/shipment-/${tracking_code}`);
  };

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <div className={classes.box}>
        <Grid container spacing={3}>
          <Grid item lg md={3} xs={6} className={classes.griditem}>
            <Button
              className={classes.paper}
              style={{ background: "#F1913D", color: "white", }}
            >
              Pending Shipments
            </Button>
          </Grid>
          <Grid item lg md={3} xs={6} className={classes.griditem}>
            <Button
              className={classes.paper}
              style={{ background: "#6C97C9", color: "white" }}
              type="button"
            >
              Processing Shipments
            </Button>
          </Grid>
          <Grid item lg md={3} xs={6} className={classes.griditem}>
            <Button
              className={classes.paper}
              style={{ background: "#6C97C9", color: "white" }}
            >
              Dispatched Shipments
            </Button>
          </Grid>
          <Grid item lg md={3} xs={6} className={classes.griditem}>
            <Button
              className={classes.paper}
              style={{ background: "#58BC34", color: "white" }}
            >
              Delivered Shipments
            </Button>
          </Grid>
          <Grid item lg md={3} xs={6} className={classes.griditem}>
            <Button
              className={classes.paper}
              style={{ background: "#ED3E3E", color: "white" }}
            >
              Rejected Shipments
            </Button>
          </Grid>
          <Grid item lg md={3} xs={6} className={classes.griditem}>
            <Button
              className={classes.paper}
              style={{ background: "#ED3E3E", color: "white" }}
            >
              Returned Shipments
            </Button>
          </Grid>
        </Grid>
      </div>
      <TableContainer component={Paper}>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Typography style={{ paddingTop: "13px" }} className={classes.typo}>
            Pending Shipment Lists
          </Typography>
          <TextField
            style={{ marginLeft: "6px" }}
            inputProps={{
              style: {
                padding: "9.5px 14px",
              },
            }}
            id="outlined-basic"
            placeholder="type keyword & enter"
            variant="outlined"
          />
        </div>
        <Divider />
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell className={classes.tablecell}>
                Tracking&nbsp;Code
              </TableCell>
              <TableCell align="left" className={classes.tablecell}>
                Name
              </TableCell>
              <TableCell align="left" className={classes.tablecell}>
                Address
              </TableCell>
              <TableCell align="left" className={classes.tablecell}>
                Contact&nbsp;Number
              </TableCell>
              <TableCell align="left" className={classes.tablecell}>
                Alternative&nbsp;Contact&nbsp;Number
              </TableCell>
              <TableCell align="left" className={classes.tablecell}>
                Cod
              </TableCell>
              <TableCell align="left" className={classes.tablecell}>
                Action
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {isLoading && <CircularProgress className={classes.loading} />}
            {shipmentList
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((shipment) => (
                <TableRow key={shipment.tracking_code} hover>
                  <TableCell component="th" scope="row">
                    {shipment.tracking_code}
                  </TableCell>
                  <TableCell align="left">{shipment.receiver_name}</TableCell>
                  <TableCell align="left">
                    {shipment.receiver_address}
                  </TableCell>
                  <TableCell align="left">
                    {shipment.receiver_contact_number}
                  </TableCell>
                  <TableCell align="left">
                    {shipment.receiver_alt_contact_number}
                  </TableCell>
                  <TableCell align="left">{shipment.cod_value}</TableCell>
                  <TableCell align="left">
                    <Button
                      style={{
                        background: "#319F8D",
                        color: "white",
                        padding: "5px",
                        fontSize: "11px",
                      }}
                      onClick={() => handleButtonClick(shipment.tracking_code)}
                    >
                      View Detail
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10]}
        component="div"
        count={shipmentList.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </main>
  );
};

export default ShipmentList;
