import React, { useEffect, useContext, useState } from "react";
import {
  makeStyles,
  Paper,
  Typography,
  TextField,
  Button,
  Divider,
  MenuItem,
  Grid,
  TextareaAutosize,
} from "@material-ui/core";
import { authContext } from "../../../App";
import { useNavigate } from "react-router-dom";
import Snackbar from "@material-ui/core/Snackbar";
import { Alert } from "@material-ui/lab";

const useStyles = makeStyles((theme) => ({
  // necessary for content to be below app bar
  toolbar: {
    minHeight: "56px",
    maxHeight: "56px",
    paddingTop: "15px",
  },
  content: {
    marginLeft: "240px",
    flexGrow: 1,
    padding: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      marginLeft: "0px",
    },
  },
  paper: {
    paddingLeft: "25px",
    paddingRight: "25px",
  },
  typo: {
    marginLeft: "25px",
    marginTop: "10px",
    fontSize: "19px",
    paddingTop: "15px",
  },
  form: {
    marginTop: "25px",
  },
  note: {
    display: "flex",
    marginTop: "15px",
  },
  container: {
    display: "flex",
    justifyContent: "space-between",
  },
  formControl: {
    width: "100%",
  },
  control: {
    width: "50%",
    [theme.breakpoints.down("xs")]: {
      width: "100%",
    },
  },
  button: {
    color: "white",
    marginTop: "25px",
    marginBottom: "20px",
    textAlign: "center",
  },
  msg: {
    width: "100%",
    marginBottom: "20px",
    [theme.breakpoints.down("xs")]: {
      width: "100%",
    },
  },
}));

const CreatePaymentRequest = ({ title }) => {
  let navigate = useNavigate();
  const classes = useStyles();
  const context = useContext(authContext);
  const [serviceProviders, setServiceProviders] = useState([]);
  const [receivingAccounts, setReceivingAccounts] = useState([]);
  const [cB, setCB] = useState([]);
  const [errorMessage, setErrorMessage] = useState("");

  const [createPaymentRequest, setCreatePaymentRequest] = useState({
    logistics_branch: null,
    preferred_receiving_account: null,
    current_balance: 0,
    message: "",
  });

  const [open, setOpen] = React.useState(false);
  const instance = context.user.axiosInstance;

  useEffect(() => {
    document.title = title;
    instance
      .get(
        `/api/v1/customer/payment-req-service-provider-list/`
      )
      .then((res) => {
        setServiceProviders(res.data.data);
      })
      .catch((err) => console.log(err));

    instance
      .get(
        `/api/v1/customer/payment-req-receiving-account-list/`
      )
      .then((res) => {
        setReceivingAccounts(res.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const handleOnChange = (e) => {
    // setSelectedSP(e.target.value)
    const value = e.target.value;
    setCreatePaymentRequest({
      ...createPaymentRequest,
      logistics_branch: value,
    });

    instance
      .get(
        `/api/v1/customer/payment-request-current-balance-${e.target.value}/`
      )
      .then((res) => {
        // setCurrentBalance(res.data.data);
        // setCB(res.data);
        setCreatePaymentRequest({
          ...createPaymentRequest,
          current_balance: res.data.data,
          logistics_branch: value,
        });
        setCB(res.data);
      })
      .catch((err) => console.log(err));
  };

  const handleButtonClick = (e) => {
    e.preventDefault();
    // if (selectedSP && selectedRA) {
    if (
      createPaymentRequest.logistics_branch &&
      createPaymentRequest.preferred_receiving_account
    ) {
      instance
        .post(
          `/api/v1/customer/payment-request-create/`,
          // { selectedRA, selectedSP, message },
          createPaymentRequest
          
        )
        .then((res) => {
          if (res.data.status === "failure") {
            setErrorMessage(res.data.message);
          } else if (res.data.status === "success") {
            setOpen(true);
            setTimeout(function () {
              navigate("/payment-list");
            }, 1000);
          }
        })
        .catch((err) => console.log(err));
    }
  };
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <Paper className={classes.paper}>
        <Typography variant="h6" display="initial" className={classes.typo}>
          REQUEST COD Payment
        </Typography>

        <small
          style={{
            display: "inline-block",
            color: "red",
            marginLeft: "25px",
            marginBottom: "35px",
          }}
        >
          Fields marked with '*' are required fields.
        </small>

        <Divider />
        <form className={classes.form} onSubmit={handleButtonClick}>
          <Grid container spacing={4} xs>
            <Grid item xs={12} sm={8}>
              <Grid item xs={12} sm={12} lg={12}>
                <TextField
                  id="select"
                  onChange={handleOnChange}
                  label=" Select Service Provider:"
                  variant="outlined"
                  margin="normal"
                  // value={selectedSP}
                  value={createPaymentRequest.logistics_branch}
                  select
                  required
                  className={classes.formControl}
                >
                  {serviceProviders.map((data) => (
                    <MenuItem key={data.id} value={data.id}>
                      {data.branch_city} ({data.logistics_company})
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>

              <Grid item xs={12} sm={6} lg={12}>
                <TextField
                  id="select"
                  onChange={(e) =>
                    setCreatePaymentRequest({
                      ...createPaymentRequest,
                      preferred_receiving_account: e.target.value,
                    })
                  }
                  label=" Preferred Receiving Account:"
                  variant="outlined"
                  margin="normal"
                  value={createPaymentRequest.preferred_receiving_account}
                  select
                  required
                  className={classes.formControl}
                >
                  {receivingAccounts.map((data) => (
                    <MenuItem key={data.id} value={data.id}>
                      {data.account_in}-{data.account_number} (
                      {data.account_name})
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>

              <Grid item xs={12} sm={6} lg={12}>
                <Typography>Your Message</Typography>
                <TextareaAutosize
                  className={classes.msg}
                  aria-label="minimum height"
                  minRows={5}
                  value={createPaymentRequest.message}
                  onChange={(e) =>
                    setCreatePaymentRequest({
                      ...createPaymentRequest,
                      message: e.target.value,
                    })
                  }
                />
              </Grid>
            </Grid>

            <Grid item xs={12} sm={4}>
              <Paper
                variant="outlined"
                square
                style={{ borderColor: "#319F8D", padding: " 20px" }}
              >
                CURRENT BALANCE WITH <br />
                {createPaymentRequest.current_balance.branch_city} (
                {createPaymentRequest.current_balance.logistics_company}) )
                <br />
                <br />
                <br />
                SHIPMENT COUNT: {cB.shipment_count} <br />
                <br />
                CURRENT BALANCE: Rs. {cB.current_balance} /-
                <br />
                <br />
                <span style={{ color: "red" }}>{errorMessage}</span>
                <br />
                <br />
              </Paper>
            </Grid>
          </Grid>

          <Button
            variant="contained"
            style={{ background: "#319F8D", color: "white" }}
            className={classes.button}
            type="submit"
          >
            Request Payment
          </Button>
          {open && (
            <>
              <Snackbar
                open={open}
                onClose={handleClose}
              >
                <Alert onClose={handleClose} severity="success">
                  Your Payment Request has been Created!
                </Alert>
              </Snackbar>
            </>
          )}
        </form>
      </Paper>
    </main>
  );
};

export default CreatePaymentRequest;
